<?php
/**
 * http://java.dzone.com/articles/design-patterns-strategy
 *
 * Шаблонът Стратегия се използва в случаите, когато е необходим динамичен избор на алгоритъм. Дефинира се семейство от различни,
 * но взаимозаменяеми алгоритми, всеки един от тях капсулиран като отделни класове.
 * Идеален примерно за писане на валидиращи скриптове за различни типове променливи - примерно, ако е integer, валидирай го с този клас,
 * ако е дата/време - с този клас...
 * Което значи, че трябва да имаш различни класове, които да правят различните валидации.
 * Всеки такъв клас трябва да имплементира един общ интерфейс, за да са взаиозаменяеми.
 * Цялата работа е да се избегне предефинирането (overriding) на една и съща логика (метод) във всички наследници.
 * According to the Strategy Pattern, the behaviors of a class should not be inherited. Instead they should be encapsulated using interfaces.
 *
 * Имаш отделни алгоритми, капсулирани в класове и инплементиращи даден(и) интерфейс(и) за да имат нещо общо помежду си.
 * Това са ти отделните стратегии.
 * Имаш отделно и друг клас (да го наречем "клиент"), който ги използва. По-точно използва този клас-стратегия, който му трябва според случая.
 * Aко имаш да променяш нещо, променяй класовете, не и клиентът, който ги вика.
 * Това е всъщност Open-Close Principle.
 */

declare(strict_types = 1);

interface IStrategy
{
	public function showTitle(Book $oBook) : string;
}

class StrategyCaps implements IStrategy
{
	public function showTitle(Book $oBook) : string
	{
		$title = $oBook->getTitle();
		return strtoupper($title);
	}
}

class StrategyExclaim implements IStrategy
{
	public function showTitle(Book $oBook) : string
	{
		$title = $oBook->getTitle();
		return str_replace(' ', '!', $title);
	}
}

class StrategyStars implements IStrategy
{
	public function showTitle(Book $oBook) : string
	{
		$title = $oBook->getTitle();
		return str_replace(' ', '*', $title);
	}
}


class StrategyContext
{
	private IStrategy $strategy;	// Instance of one of the above classes

	// 1. Set desired strategy in a private property
	public function __construct(IStrategy $oStrategy)
	{
		$this->strategy = $oStrategy;
	}

	// 2. Perform selected strategy method
	public function showBookTitle($book) : string
	{
		return $this->strategy->showTitle($book);
	}
}


class Book
{
	private string $author, $title;

	public function __construct(string $title_in, string $author_in)
	{
		$this->author = $author_in;
		$this->title = $title_in;
	}

	public function getAuthor() : string
	{
		return $this->author;
	}

	public function getTitle() : string
	{
		return $this->title;
	}

	public function getAuthorAndTitle() : string
	{
		return $this->getTitle() . ' by ' . $this->getAuthor();
	}
}


// Предварително създаваме набор от различни стратегии
$strategyContextC = new StrategyContext(new StrategyCaps());
$strategyContextE = new StrategyContext(new StrategyExclaim());
$strategyContextS = new StrategyContext(new StrategyStars());

// Върху този обект ще прилагаме различните стратегии
$book = new Book('Nemili Nedragi', 'Ivan Vazov');

echo "\n\ntest 1 - show name context C \n";
echo $strategyContextC->showBookTitle($book);
echo "\n\n";

echo "\n\ntest 2 - show name context E \n";
echo $strategyContextE->showBookTitle($book);
echo "\n\n";

echo "\n\ntest 3 - show name context S \n";
echo $strategyContextS->showBookTitle($book);
echo "\n\n";


// Може и без контекст
$strategyContextC1 = new StrategyCaps();
echo $strategyContextC1->showTitle($book);



echo "\n\n\n**************************************************\n\n\n";



interface IStrateg
{
	public function validate(string $val) : bool;
}

class ValidInteger implements IStrateg
{
	public function validate(string $val = '') : bool
	{
		if(($ret = preg_match('/\d+/', $val)) === false){
			throw new Exception('Boom on line ' . __LINE__);
		}
		return (bool)$ret;
	}
}

class ValidateString implements IStrateg
{
	public function validate(string $val = '') : bool
	{
		return (bool)strlen(trim($val));
	}
}

class ValidateDate implements IStrateg
{
	public function validate(string $val = '') : bool
	{
		if(($ret = preg_match('/\d{4}-\d{1,2}-\d{1,2}/', $val)) === false){
			throw new Exception('Boom on line ' . __LINE__);
		}
		return (bool)$ret;
	}
}

class Context
{
	private IStrateg $strategy;

	public function __construct(IStrateg $strategy)
	{
		$this->strategy = $strategy;
	}

	public function validate(string $val) : bool
	{
		return $this->strategy->validate($val);
	}
}

try {
	$val = '123';
	$context = new Context(new ValidInteger());
	var_dump($context->validate($val));

	$val = 'hello plamen';
	$context = new Context(new ValidateString());
	var_dump($context->validate($val));

	$val = '2015-01-19';
	$context = new Context(new ValidateDate());
	var_dump($context->validate($val));
} catch (Exception $e){
	echo $e->getMessage();
}



echo "\n\n\n************************ROBOTI**************************\n\n\n";



interface iBehaviour
{
	public function meetAnotherRobot();
}

class beAgressive implements iBehaviour
{
	public function meetAnotherRobot(){
		return 'I will kill you!!!';
	}
}

class beCoward implements iBehaviour
{
	public function meetAnotherRobot(){
		return 'No, please do not kill me!!!';
	}
}

class beFriendly implements iBehaviour
{
	public function meetAnotherRobot(){
		return 'Hi, how are you!!!';
	}
}


class Robot
{
	private $robotName = null;
	private $objRobotBehaviour = null;

	public function __construct($name){
		$this->robotName = $name;
	}

	public function getName(){
		return $this->robotName;
	}
	public function setName($name){
		$this->robotName = $name;
	}

	public function setRobotBehaviour(iBehaviour $objRobotBehaviour){
		$this->objRobotBehaviour = $objRobotBehaviour;
	}

	public function getRobotBehaviour(){
		return $this->objRobotBehaviour;
	}

	public function move(){
		echo $this->getName() . ' is moving and has found another robot.' . "\n";
		echo $this->getName() . ' says: ' . $this->getRobotBehaviour()->meetAnotherRobot() . "\n";
		echo 'Thats it.';
	}
}


$r1 = new Robot('Big Robot');
$r2 = new Robot('George v.2.1');
$r3 = new Robot('R2-D2');

$r1->setRobotBehaviour(new beCoward());
$r3->setRobotBehaviour(new beFriendly());

$r1->move();
