<?php
/**
 * "Attach additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality."
 * (Gang Of Four)
 *
 * Примерът с пиците: искам в моята пицария да предлагам примерно 10 вида пици. И си правя един абстрактен клас Pizza, и 10 наследника -
 * по един за всеки вид пица. И във всеки клас-пица задавам твърдо цената в зависимост какви са съставките на дадената пица.
 * QuattroStagioni ще струва 10.99 защото в нея има пармезан, зехтин, пипер, домат, маслини.
 * VecchiaRoma ще струва 9.10 защото има само ементал, зехтин и маслини...
 * Добре, обаче какво става ако примерно маслините поскъпнат. Трябва на всеки клас-пица да мина и да сметна наново цените.
 * Варианти много разбира се, може цената на всяка съставка да си я отделя като константа, или пък ....
 *
 * А може и така: за всяка съставка (а не за всеки модел пица) създаваш по един клас където в едно protected пропърти да е цената.
 */

class BookComponent
{
	private string $author, $title;

	public function __construct(string $title_in = '', string $author_in = '')
	{
		$this->author = $author_in;
		$this->title  = $title_in;
	}

	public function getAuthor() : string 
	{
		return $this->author;
	}

	public function getTitle() : string
	{
		return $this->title;
	}

	public function setTitle(string $title) : void
	{
		$this->title = $title;
	}
}


abstract class BookTitleDecorator
{
	protected BookComponent $bookComponent;
	protected string $title;

	public function __construct(BookComponent $bookComponent)
	{ 
		$this->bookComponent = $bookComponent;
		$this->resetTitle();
	}

	// Doing this so original object is not altered
	public function resetTitle() : void
	{
		$this->title = $this->bookComponent->getTitle();
	}

	public function getTitle() : string
	{
		return $this->title;
	}

	abstract public function decorate() : void;
}


class BookTitleExclaimDecorator extends BookTitleDecorator
{
	public function decorate() : void
	{
		$newTitle = '!' . $this->bookComponent->getTitle() . '!';
		$this->bookComponent->setTitle($newTitle);
	}
}


class BookTitleStarDecorator extends BookTitleDecorator
{
	public function decorate() : void
	{
		$newTitle = str_replace(' ', '*', $this->bookComponent->getTitle());
		$this->bookComponent->setTitle($newTitle);
	}
}


$patternBook = new BookComponent('Gamma, Helm, Johnson, and Vlissides', 'Design Patterns');

$starDecorator = new BookTitleStarDecorator($patternBook);
$exclaimDecorator = new BookTitleExclaimDecorator($patternBook);

echo "Showing title : \n";
echo $patternBook->getTitle();
echo "\n\n";

echo "Showing title after two exclaims added : \n";
$exclaimDecorator->decorate();
$exclaimDecorator->decorate();
echo $exclaimDecorator->getTitle();
echo "\n\n";

echo "Showing title after star added : \n";
$starDecorator->decorate();
echo $starDecorator->getTitle();
echo "\n\n";

echo "Showing title after reset: \n";
echo $starDecorator->resetTitle();
echo $starDecorator->getTitle();





echo "\n\n\n**************************************************\n\n\n";





abstract class IIngr
{
	private ?self $nextIngr = NULL;
	protected float $cost = 0.00;

	public function __construct(self $i = NULL)
	{
		$this->nextIngr = $i;
	}

	public function getCost() : float
	{
		return $this->cost +
			($this->nextIngr 
				? $this->nextIngr->getCost() 
				: 0);
	}
}

class Salami extends IIngr {
	protected float $cost = 1.50;
}
class Ementaler extends IIngr {
	protected float $cost = 1.80;
}
class Gouda extends IIngr {
	protected float $cost = 1.90;
}
class FettaCheese extends IIngr {
	protected float $cost = 20.50;
}
class Pizza extends IIngr {
	protected float $cost = 0.25;
}

$p = new Pizza(
	new Salami(
		new Gouda(
			new FettaCheese
		)
	)
);

echo $p->getCost();
