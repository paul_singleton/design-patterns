<?php
/**
 * "Convert the interface of a class into another interface clients expect. Adapter lets classes work together that couldn't otherwise
 * because of incompatible interfaces." (Gang Of Four)
 *
 * Подходящ е когато искаме някой клас, който не можем или не искаме да редактираме, да му добавим свои методи.
 * Правим си друг клас - адаптер, в конструктора му предаваме обект от класа, който искаме да "разширим", и пишем метод(и),
 * в които използваме методите му (на "разширявания").
 * Така можеш лесно да "преоблечеш" даден клас. Подаваш му обекта на друг клас (адаптера) и там му ползваш public методите и пропъртита
 * за да сглобиш друга логика.
 * То затова и този ДП се казва Адаптер. Когато имаш клас, който не ти върши точно тази работа, която ти трябва, да си напишеш друг, 
 * който да използва
 * public методите и пропъртита на първия.
 *
 * Правим си друг клас - адаптер, в конструктора му предаваме обект от класа, който искаме да "адаптираме", и пишем метод(и),
 * в които използваме методите му (на "адаптираният").
 * Така можеш лесно да "преоблечеш" даден обект го на друг обект (адаптера) и там му ползваш public методите и пропъртита
 * за да сглобиш друга логика.
 *
 * Важно е да се знае, че Адаптер не добавя нови методи към адаптираният клас, това е Декоратор.
 * Адаптер само напасва. Или съвсем просто казано, вместо да викаме нещо директно, викаме нещо друго, което вика нещото.
 * И това се налага най-често
 * не за да променяме или добавяме логика, а например за да напаснем различни сигнъчъри.
 *
 * Разликата между Адаптер, Фасада, Прокси и Декоратор:
 * Фасада - имаме сложна система между класове и искаме да имаме лесен начин да си взаимодействаме с тях. Фаасадата стои пред тях
 * и контактуваме с нея.
 * Фасадата на практика крие тази комплексна логика, пред която стои.
 *
 * Прокси - създаване на клас-прокси между теб и класът, с който искаш да работиш и сместо да викаш нещото, което искаш да извикаш,
 * викаш проксито.
 * Използва се например когато искаме с някаква логика да проверим дали имаме право да работим с даденият клас.
 *
 * Декоратор - начин за добавяне на логика към даден клас, без да го променяме. Демек, украсяваме клас.
 */

interface ITarget
{
	public function request();
}

class Adaptee
{
	public function specificRequest()
	{
		// тук е оригиналната логика на метода, който ще адаптираме.
	}
}

class Adapter implements ITarget
{
	private Adaptee $adaptee;

	public function __construct(Adaptee $adaptee)
	{
		$this->adaptee = $adaptee;
	}

	public function request()
	{
		// тук викаме специфичната логика, която ще адаптираме - $this->adaptee->specificRequest();
		// и пишем логиката за адаптиране. Демек, тук става самото адаптиране.
	}
}

$adapted = new Adapter(new Adaptee);
$adapted->request();
