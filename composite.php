<?php
/**
 * "Compose objects into tree structures to represent part-whole hierarchies.
 * Composite lets clients treat individual objects and compositions of objects uniformly."
 * (Gang Of Four)
 *
 * Подходящ когати искаме да направим нещо като структура от обекти, които да се съдържат в по-голям обект, и големите - в още по-голям и т.н...
 * Примерно работници (малките обекти) във фирма (големият обект), песни в музикален албум...
 */

abstract class BookShelf
{
	abstract function getBookInfo($previousBook);

	abstract function getBookCount();
	abstract function setBookCount($new_count);

	abstract function addBook($oneBook);
	abstract function removeBook($oneBook);
}


class OneBook extends BookShelf
{
	private $title, $author;

	public function __construct($title, $author){
		$this->title = $title;
		$this->author = $author;
	}

	public function getBookInfo($booksToGet){
		if($booksToGet == 1){
			return $this->title . ' by ' . $this->author;
		} else {
			return false;
		}
	}
	public function getBookCount(){
		return 1;
	}
	public function setBookCount($newCount){
		return false;
	}
	public function addBook($oneBook){
		return false;
	}
	public function removeBook($oneBook){
		return false;
	}
}


class SeveralBooks extends BookShelf
{
	private $oneBooks = array();
	private $bookCount;

	public function __construct(){
		$this->setBookCount(0);
	}

	public function getBookCount(){
		return $this->bookCount;
	}
	public function setBookCount($newCount){
		$this->bookCount = $newCount;
	}

	public function getBookInfo($bookToGet){
		if($bookToGet <= $this->bookCount){
			return $this->oneBooks[$bookToGet]->getBookInfo(1);
		} else {
			return false;
		}
	}

	public function addBook($oneBook){
		$this->setBookCount($this->getBookCount() + 1);
		$this->oneBooks[$this->getBookCount()] = $oneBook;
		return $this->getBookCount();
	}

	public function removeBook($oneBook){
		$counter = 0;
		while(++$counter <= $this->getBookCount()){
			if($oneBook->getBookInfo(1) == $this->oneBooks[$counter]->getBookInfo(1)){
				for($x = $counter; $x < $this->getBookCount(); $x++){
					$this->oneBooks[$x] = $this->oneBooks[$x + 1];
				}
				$this->setBookCount($this->getBookCount() - 1);
			}
		}
		return $this->getBookCount();
	}
}


$firstBook = new OneBook('"Core PHP Programming, Third Edition"', '"Atkinson and Suraski"');
echo "(after creating first book) oneBook info: \n";
echo $firstBook->getBookInfo(1);
echo "\n\n";

$secondBook = new OneBook('"PHP Bible"', '"Converse and Park"');
echo "(after creating second book) oneBook info: \n";
echo $secondBook->getBookInfo(1);
echo "\n\n";

$thirdBook = new OneBook('"Design Patterns"', '"Ivan Parmakov"');
echo "(after creating third book) oneBook info: \n";
echo $thirdBook->getBookInfo(1);
echo "\n\n\n\n";


$books = new SeveralBooks();


$booksCount = $books->addBook($firstBook);
echo "(after adding firstBook to books) SeveralBooks info : \n";
echo $books->getBookInfo($booksCount);
echo "\n\n";

$booksCount = $books->addBook($secondBook);
echo "(after adding secondBook to books) SeveralBooks info : \n";
echo $books->getBookInfo($booksCount);
echo "\n\n";

$booksCount = $books->addBook($thirdBook);
echo "(after adding thirdBook to books) SeveralBooks info : \n";
echo $books->getBookInfo($booksCount);
echo "\n\n";

$booksCount = $books->removeBook($firstBook);
echo "(after removing firstBook from books) SeveralBooks count : \n";
echo $books->getBookCount();
echo "\n\n";

echo "(after removing firstBook from books) SeveralBooks info 1 : \n";
echo $books->getBookInfo(1);
echo "\n\n";

echo "(after removing firstBook from books) SeveralBooks info 2 : \n";
echo $books->getBookInfo(2);



echo "\n\n\n**************************************************\n\n\n";



/**
 * Ще пресъздадем проста структура на фирма с различни отдели и един шеф. Във всеки отдел има N на брой работници и всеки с някаква заплата,
 * шефа също ще го броим за работник, просто заплатата му ще е по-голяма. И той няма да се води към никой отдел.
 * Имаш прост клас Rabotnik, който има заплата ($salary). Имаш друг клас Otdel, на който подаваш масив от обекти на Rabotnik (работниците).
 * И трябва да сметнем колко пари трябват всеки месец за заплати - просто да съберем на всички заплатите.
 */

interface ICompanyPart {
	public function getCost();
}

class Rabotnik implements ICompanyPart
{
	private $salary = 0.00;

	public function __construct($salary){
		$this->salary = $salary;
	}
	public function getCost(){
		return $this->salary;
	}
}

class Otdel implements ICompanyPart
{
	private $children = NULL;

	public function __construct($aRabotnici){
		$this->children = $aRabotnici;
	}
	public function getCost(){
		$cost = 0;
		foreach($this->children as $v){
			$cost += $v->getCost();
		}
		return $cost;
	}
}

$otdelSchetovodstvo = new Otdel(
	array(
		new Rabotnik(200),
		new Rabotnik(250)
	)
);

$otdelLichenSastav = new Otdel(
	array(
		new Rabotnik(150),
		new Rabotnik(170),
		new Rabotnik(140)
	)
);

$Shefa = new Rabotnik(1000);

$company = array($otdelSchetovodstvo, $Shefa, $otdelLichenSastav);
$a = new Otdel($company);
echo $a->getCost();
