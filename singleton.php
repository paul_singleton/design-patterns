<?php
/**
 * Използва се техниката "lazy instantiation", т.е. ако нямаме нужда от обекта, не го създаваме.
 * И инстанцията на дадения клас трябва да се съхранява в статична, private променлива, която достигаме чрез статичен метод - access point.
 * В този метод (access point) проверяваме дали имаме вече инстанция, ако да - връщаме я. Ако не - създаваме я и пак я връщаме. Прис следващо извикване на
 * access point-a, вече ще я имаме (инстанцията) и само я връщаме.
 * В примера с книгите имаме клас за книга BookSingleton, където е сингълтън-а, и идеята е когато заемаш книгата от библиотеката,
 * после друг да не може да я взема. Класът е САМО ЗА ЕДНА КОНКРЕТНА книга (Ivan Vazov - Nemili Nedragi)
 */

class BookSingleton
{
	private string $author = 'Ivan Vazov';
	private string $title  = 'Nemili Nedragi';

	static private ?self $book = NULL;

	static private bool $isLoanedOut = FALSE;

	private function __construct(){}
	private function __clone(){}

	static public function borrowBook() : ?self
	{
		if (FALSE === self::$isLoanedOut) {
			if (NULL === self::$book) {
				self::$book = new BookSingleton();
			}
			self::$isLoanedOut = TRUE;
			return self::$book;
		}

		return NULL;
	}

	public function returnBook(BookSingleton $bookReturned) : void
	{
		self::$isLoanedOut = FALSE;
	}

	private function getAuthor() : string
	{
		return $this->author;
	}

	private function getTitle() : string
	{
		return $this->title;
	}

	public function getAuthorAndTitle() : string
	{
		return $this->getTitle() . ' by ' . $this->getAuthor();
	}
}


class BookBorrower
{
	private ?BookSingleton $borrowedBook;
	private bool $haveBook = FALSE;

	public function __construct(){}

	public function borrowBook() : void
	{
		$this->borrowedBook = BookSingleton::borrowBook();
		$this->haveBook = ($this->borrowedBook === NULL);
	}

	public function getAuthorAndTitle() : string
	{
		if ((TRUE === $this->haveBook) && $this->borrowedBook) {
			return $this->borrowedBook->getAuthorAndTitle();
		}
		return "I don't have the book";
	}

	public function returnBook() : void
	{
		$this->borrowedBook->returnBook($this->borrowedBook);
	}
}


$bookBorrower1 = new BookBorrower();
$bookBorrower2 = new BookBorrower();

$bookBorrower1->borrowBook();
echo "Book Borrower1 asked to borrow the book \n";
echo "Book Borrower1 Author and Title: \n";
echo $bookBorrower1->getAuthorAndTitle() . "\n";
echo "\n\n";

$bookBorrower2->borrowBook();
echo "Book Borrower2 asked to borrow the book \n";
echo "Book Borrower2 Author and Title: \n";
echo $bookBorrower2->getAuthorAndTitle() . "\n";
echo "\n\n";

$bookBorrower1->returnBook();
echo "Book Borrower1 returned the book \n";
echo "\n\n";

$bookBorrower2->borrowBook();
echo "Book Borrower2 Author and Title: \n";
echo $bookBorrower1->getAuthorAndTitle() . "\n";
echo "\n\n";
