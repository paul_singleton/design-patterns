<?php
/**
 * Подходящ е когато трябва runtime да се променя логиката на даден клас. С него можеш да си реализираш цяла логика вътре в класа.
 * Трябва предварително да си определиш различните възможни състояния, и действията зад всяко състояние. И какво да прави класа при всяко от тях.
 * Примерно, всяко състояние може да ти е някаква константа с integer, трябва да имаш и обектна променлива, където да ти е текущото състояние.
 */

interface IBookTitleState {
	public function showTitle($oContext);
}


class BookTitleStateStars implements IBookTitleState
{
	private $titleCount = 0;

	public function showTitle($oContext){
		$title = $oContext->getBook()->getTitle();
		$this->titleCount++;
		if($this->titleCount > 1){
			$oContext->setTitleState(new BookTitleStateExclaim);
		}
		return str_replace(' ', '*', $title);
	}
}


class BookTitleStateExclaim implements IBookTitleState
{
	private $titleCount = 0;

	public function showTitle($oContext){
		$title = $oContext->getBook()->getTitle();
		$this->titleCount++;
		$oContext->setTitleState(new BookTitleStateStars());
		return str_replace(' ', '!', $title);
	}
}


class BookContext
{
	private $book = NULL;
	private $bookTitleState = NULL;

	public function __construct($oBook){
		$this->book = $oBook;
		$this->setTitleState(new BookTitleStateStars());
	}
	public function setTitleState($oTitleState){
		$this->bookTitleState = $oTitleState;
	}
	public function getBookTitle(){
		return $this->bookTitleState->showTitle($this);
	}
	public function getBook(){
		return $this->book;
	}
}


class Book
{
	private $title, $author;

	public function __construct($title = '', $author = ''){
		$this->author = $author;
		$this->title = $title;
	}

	public function getAuthor(){
		return $this->author;
	}
	public function getTitle(){
		return $this->title;
	}
}


$book = new Book('Brideshead Revisited', 'Evelyn Waugh');
$context = new BookContext($book);

echo "Test 1 - show name \n";
echo $context->getBookTitle();
echo "\n\n";

echo "Test 2 - show name \n";
echo $context->getBookTitle();
echo "\n\n";

echo "Test 3 - show name \n";
echo $context->getBookTitle();
echo "\n\n";

echo "Test 4 - show name \n";
echo $context->getBookTitle();
echo "\n\n";
