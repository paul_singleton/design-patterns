<?php

var_dump(WApp::AString()->genRandStr());
var_dump(WApp::AString()->encodeStr('plamen'));
var_dump(WApp::AString()->genRandStr());

var_dump(WApp::ADatetime()->getServerDatetime());
var_dump(WApp::ADatetime()->getServerMonth());

var_dump(WApp::ADatetime()->getServerMonth());
var_dump(WApp::ADatetime()->getServerMonth());
var_dump(WApp::ADatetime()->getServerMonth());


class WApp
{
	static private $aObjPool = array();		// Objects pool

	protected function __construct(){}
	protected function __clone(){}
	protected function __wakeup(){}

	static public function __callStatic($className, $args){
		//var_dump($className, $args);
		if(empty(self::$aObjPool[$className])){
			self::$aObjPool[$className] = new $className($args);
			echo " W \n";
		}
		return self::$aObjPool[$className];
	}
}

class AString
{
	public function __construct(){
		echo ' CONSTRUCTOR -> AString ';
	}
	public function genRandStr($len = 10){
		return md5(rand() . time() . rand() . microtime() . rand());
	}
	public function encodeStr($str = ''){
		return md5($str);
	}
	public function escapeHtml($str = '', $charset = 'utf-8'){
		return htmlentities($str, ENT_QUOTES, $charset);
	}
}

class ADatetime
{
	public function __construct(){
		echo ' CONSTRUCTOR -> ADatetime ';
	}
	public function getServerDatetime($format = 'd M Y h:i:s A'){
		return date($format);
	}
	public function getServerDay($format = 'd'){
		return date($format);
	}
	public function getServerMonth($format = 'M'){
		return date($format);
	}
}

class AValidator
{
	public function __construct(){
		echo ' CONSTRUCTOR -> AValidator ';
	}
	public function isValidEmail($email = ''){
		return (bool)filter_var($email, FILTER_VALIDATE_EMAIL);
	}
	public function isInt($val = ''){
		return (bool)preg_match('/\d+/', $val);
	}
}
