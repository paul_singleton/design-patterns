<?php
/**
 * http://java.dzone.com/articles/design-patterns-visitor
 *
 * Използва се когато искаш да дефинираш дадени действия за даден обект, които действия да са дефинирани НЕ В НЕГО,
 * а в някой друг (т.н. Visitor). Което позволява без да "раздуваме" даденият клас прекалено много, динамично да 
 * добавяме още функционалност, но в други класове - Посетители.
 * Т.е. да добавиш допълнителна функционалност към клас без да го променяш него самия.
 * Примерно ако искаш да имаш много еднакви или близки (не е задължително) като структура обекти,
 * но отделно да може да имаш различни действия за всеки от тях.
 * И за да ги запазиш еднакви като структура, може да отделиш тези различни действия в Visitor (или много Visitor-и).
 * И само да му предаваш (с $this) въпросният обект, и той (Visitor-a) изпозва неговите (на предадения обект) методи 
 * или променливи (които естествено трябва да са public). Но разбира се, Visitor-а трябва първо да разбере типа (класа)
 * на обекта, който му предаваш.
 * Visitor-a само приема различни обекти, и според това от кой клас са, прави различни неща с тях. Visitor-а има 
 * по една функция за всеки клас, който му подават (поне в Java, защото има overloading, а в PHP няма overloading 
 * и става малко по-заобиколно).
 *
 * Създаваш един или повече обекти от клас ElementAbstract (напр. $vodka или $milk), със accept() вземаш обект на някой 
 * от визитърите (напр. $taxCalc или $taxHolidayCalc), и в този метод accept(), на който си му предал обект 
 * на някой от визитърите, викаш метода visit() на визитъра, с който пък предаваш с $this текущия обект ($vodka или $milk)
 * Вече в visit() метода на дадения визитор, според подадения му обект (името на класа, т.е. типа му), 
 * прави нещо с него.
 *
 * Във Visitor-а трябва да има код, който да взема типа (т.е. класа) на обекта, който му се подава, и за всеки клас
 * да има обработчик.
 *
 * Може да имаш интерфейса VisitorInterface и абстрактен клас ElementAbstract.
 * Всички класове, на които ще се подават създадените обекти, трябва да имплементират VisitorInterface, а тези които
 * ще му бъдат подавани за обработка - да наследяват ElementAbstract.
 * Във Visitor да има само 1 метод - visit(), на който ще се подава като параметър всеки от обектите 
 * за обработка (ElementAbstract).
 *
 * В Java е по-елегантно, защото може да overload-ваш методи в даден интерфейс или клас. За всеки клас, който трябва
 * да може Visitor-a да обработва, дефиниташ по един метод visit() и кой от тях ще се извика зависи от това 
 * какъв обект (от кой клас) ще му се подаде.
 * Фактически този visit() метод, (вече дефиниран в класа имплементиращ интерфейса Visitor, примерно TaxVisitor...),
 * действа като "гара разпределителна", подаваш му обект и според това от кой тип (т.е. клас) е, се изпълнява този,
 * който е за конкретния тип (клас).
 *
 * В PHP не можеш да имаш интерфейс/клас методи с еднакво име но различни входни параметри. Затова трябва да имаш един
 * метод visit(), на който подаваш всеки от обектите за обработка, и в него ще вземаш името на класа му и в зависимост
 * от името, правиш каквото трябва с този обект.
 *
 * Задачата на accept() е да предаде на Visitor-a инстанция на своя обект. Където Visitor-a да определи от кой клас
 * е този обект и да извърши дадени действия според това.
 *
 * Всеки Visitor е все едно набор от действия, които могат да се извършат върху дадени обекти от дадени класове.
 * На ЕДИН визитър може да предаваш МНОГО различни обекти от различни класове и там вече се взема типа (класа) на
 * обекта и така се вижда какво да се направи с този обект в зависимост от типа (класа) му.
 * И понеже може да имаш много Visitor-и значи имаме релация МНОГО към МНОГО, примерно водката искаш да и изчислиш
 * цената с данъци, друг път без данъци, млякото - също...
 */

declare(strict_types = 1);

interface VisitorInterface
{
	//public function visit(Liquor $liqourItem);
	//public function visit(Tobacco $tobaccoItem);
	//public function visit(Necessity $necessityItem);

	// Може една ф-я за всички ElementAbstract обекти,
	// където с get_class() да се взема името на класа на дадения ElementAbstract обекти и това да определя 
	// какво да се прави.
	public function visit(ElementAbstract $objItem) : float;

	// Може и по една ф-я за всеки тип ElementAbstract обект
	//public function visitLiquor(ElementAbstract $objItem);
	//public function visitTobacco(ElementAbstract $objItem);
	//public function visitNecessity(ElementAbstract $objItem);
}

class TaxVisitor implements VisitorInterface
{
	/*public function visit(Liquor $liqourItem){
		return ($liqourItem->getPrice() * 2) + $liqourItem->getPrice();
	}
	public function visit(Tobacco $tobaccoItem){
		return ($tobaccoItem->getPrice() * 3) + $tobaccoItem->getPrice();
	}
	public function visit(Necessity $necessityItem){
		return ($necessityItem->getPrice() * 4) + $necessityItem->getPrice();
	}*/

	public function visit(ElementAbstract $objItem) : float
	{
		switch (get_class($objItem)) {
			case 'Liquor':
				$tax = 2.43;
				break;
			case 'Tobacco':
				$tax = 3.11;
				break;
			case 'Necessity':
				$tax = 4.54;
				break;
			default:
				$tax = 0;
				break;
		}
		return ($objItem->getPrice() * $tax) + $objItem->getPrice();
	}
}

class TaxHolidayVisitor implements VisitorInterface
{
	/*public function visit(Liquor $liqourItem){
		return ($liqourItem->getPrice() * 2) + $liqourItem->getPrice();
	}
	public function visit(Tobacco $tobaccoItem){
		return ($tobaccoItem->getPrice() * 3) + $tobaccoItem->getPrice();
	}
	public function visit(Necessity $necessityItem){
		return ($necessityItem->getPrice() * 4) + $necessityItem->getPrice();
	}*/

	public function visit(ElementAbstract $objItem) : float
	{
		switch (get_class($objItem)) {
			case 'Liquor':
				$tax = 12.02;
				break;
			case 'Tobacco':
				$tax = 13.43;
				break;
			case 'Necessity':
				$tax = 14.13;
				break;
			default:
				$tax = 0;
				break;
		}
		return ($objItem->getPrice() * $tax) + $objItem->getPrice();
	}
}



abstract class ElementAbstract
{
	/**
	 * "accept()" метода е все едно входно пристанище за обектите-наследници на ElementAbstract 
	 * (тези, които ще бъдат посещавани).
	 * На този метод му се подава инстанция на VisitorInterface-а - някой от конкретните "посетители".
	 */
	public function accept(VisitorInterface $visitor) : float
	{
		return $visitor->visit($this);
	}
}

class Liquor extends ElementAbstract
{
	private float $price;

	public function __construct(float $price)
	{
		$this->price = $price;
	}

	public function getPrice() : float
	{
		return $this->price;
	}

	/*public function accept(VisitorInterface $visitor){	// Тези съм ги изнесъл в абстрактния клас
		return $visitor->visit($this);
	}*/
}

class Tobacco extends ElementAbstract
{
	private float $price;

	public function __construct(float $price)
	{
		$this->price = $price;
	}

	public function getPrice() : float
	{
		return $this->price;
	}

	/*public function accept(VisitorInterface $visitor){
		return $visitor->visit($this);
	}*/
}

class Necessity extends ElementAbstract
{
	private float $price;

	public function __construct(float $price)
	{
		$this->price = $price;
	}

	public function getPrice() : float
	{
		return $this->price;
	}

	/*public function accept(VisitorInterface $visitor){
		return $visitor->visit($this);
	}*/
}


$taxCalc = new TaxVisitor();

$vodka = new Liquor(12.50);
$cigars = new Tobacco(19.50);
$milk = new Necessity(10.50);

echo $vodka->accept($taxCalc) . "\n";
echo $cigars->accept($taxCalc) . "\n";
echo $milk->accept($taxCalc) . "\n";

echo  "\n\n\n";

$taxHolidayCalc = new TaxHolidayVisitor();

echo $vodka->accept($taxHolidayCalc) . "\n";
echo $cigars->accept($taxHolidayCalc) . "\n";
echo $milk->accept($taxHolidayCalc) . "\n";

echo  "\n\n\n";

echo $taxHolidayCalc->visit($vodka);
