<?php
/**
 * Какво е абстракция в ООП? Общите "неща" за дадена съфакупност от класове ги отделяш в един абстрактен клас, а уникалните неща ги пишеш във всеки от класовете.
 * И обикновено имаш един абстрактен клас и N на брой класове, които го екстендват.
 * Това се прави с идеята, че ако се наложи да се промени нещо в общите "неща", да не правим промяната във всички наследници, а само в родителя - абстрактния клас.
 *
 * Обаче ще имаме проблем ако се наложи да променим абстрактния клас като примерно добавим нов абстрактен метод. To decouple the abstraction
 * from the implementations so that the two can vary independently.
 * To progressively add new functionality while separating out major differences using abstract classes.
 */

//	First abstraction hierarchy BEGIN
abstract class BridgeBook
{
	private $bbAuthor, $bbTitle;
	private $bbImp;

	public function __construct($author_in = '', $title_in = '', $choice_in = ''){
		$this->bbAuthor = $author_in;
		$this->bbTitle  = $title_in;
		if('STARS' == $choice_in){
			$this->bbImp = new BridgeBookStarsImp();
		} else {
			$this->bbImp = new BridgeBookCapsImp();
		}
	}

	public function showAuthor(){
		return $this->bbImp->showAuthor($this->bbAuthor);
	}
	public function showTitle(){
		return $this->bbImp->showTitle($this->bbTitle);
	}
}

class BridgeBookAuthorTitle extends BridgeBook
{
	public function showAuthorTitle(){
		return $this->showAuthor() . "'s " . $this->showTitle();
	}
}

class BridgeBookTitleAuthor extends BridgeBook
{
	public function showTitleAuthor(){
		return $this->showTitle() . ' by ' . $this->showAuthor();
	}
}
//	First abstraction hierarchy END


//	Second abstraction hierarchy BEGIN
abstract class BridgeBookImp
{
	abstract public function showAuthor($author);
	abstract public function showTitle($title);
}

class BridgeBookCapsImp extends BridgeBookImp
{
	public function showAuthor($author_in){
		return strtoupper($author_in);
	}
	public function showTitle($title_in){
		return strtoupper($title_in);
	}
}

class BridgeBookStarsImp extends BridgeBookImp
{
	public function showAuthor($author_in){
		return str_replace(' ', '*', $author_in);
	}
	public function showTitle($title_in){
		return str_replace(' ', '*', $title_in);
	}
}
//	Second abstraction hierarchy END


echo "Test 1 - author title with caps \n";
$book = new BridgeBookAuthorTitle('Larry Truett', 'PHP for Cats', 'CAPS');
echo $book->showAuthorTitle();

echo "\n\n";

echo "Test 2 - author title with stars \n";
$book = new BridgeBookAuthorTitle('Larry Truett', 'PHP for Cats', 'STARS');
echo $book->showAuthorTitle();

echo "\n\n";

echo "Test 3 - title author with caps \n";
$book = new BridgeBookTitleAuthor('Larry Truett', 'PHP for Cats', 'CAPS');
echo $book->showTitleAuthor();

echo "\n\n";

echo "Test 4 - title author with stars \n";
$book = new BridgeBookTitleAuthor('Larry Truett', 'PHP for Cats', 'STARS');
echo $book->showTitleAuthor();



echo "\n\n\n**************************************************\n\n\n";



interface DrawingAPI {
	public function drawCircle($dX, $dY, $dRadius);
}

class DrawingAPI1 implements DrawingAPI
{
	public function drawCircle($dX, $dY, $dRadius){
		echo "API1.circle at $dX:$dY radius $dRadius. \n";
	}
}

class DrawingAPI2 implements DrawingAPI
{
	public function drawCircle($dX, $dY, $dRadius){
		echo "API2.circle at $dX:$dY radius $dRadius. \n";
	}
}

abstract class Shape
{
	protected $oDrawingAPI;

	abstract public function draw();
	abstract public function resizeByPercentage($dPct);

	protected function __construct(DrawingAPI $oDrawingAPI){
		$this->oDrawingAPI = $oDrawingAPI;
	}
}

class CircleShape extends Shape
{
	private $dX;
	private $dY;
	private $dRadius;

	public function __construct($dX, $dY, $dRadius, DrawingAPI $oDrawingAPI){
		parent::__construct($oDrawingAPI);
		$this->dX = $dX;
		$this->dY = $dY;
		$this->dRadius = $dRadius;
	}

	public function draw(){
		$this->oDrawingAPI->drawCircle(
			$this->dX,
			$this->dY,
			$this->dRadius
		);
	}

	public function resizeByPercentage($dPct){
		$this->dRadius *= $dPct;
	}
}


$aShapes = array(
	new CircleShape(1, 3,  7, new DrawingAPI1()),
	new CircleShape(5, 7, 11, new DrawingAPI2()),
);

foreach($aShapes as $shape){
	$shape->resizeByPercentage(2.5);
	$shape->draw();
}



echo "\n\n\n**************************************************\n\n\n";



abstract class EntertainmentDevice
{
	public $deviceState;
	public $maxSetting;
	public $volumeLevel = 0;

	abstract public function buttonFivePressed();
	abstract public function buttonSixPressed();

	public function deviceFeedback(){
		if(($this->deviceState > $this->maxSetting) || ($this->deviceState < 0)){
			$this->deviceState = 0;
		}
	}

	public function buttonSevenPressed(){
		$this->volumeLevel++;
		echo "Volume Up \n";
	}
	public function buttonEightPressed(){
		$this->volumeLevel--;
		echo "Volume Down \n";
	}
}


class TVDevice extends EntertainmentDevice
{
	public function __construct($newDeviceState = 0, $newMaxSetting = 0){
		$this->deviceState = $newDeviceState;
		$this->maxSetting = $newMaxSetting;
	}

	public function buttonFivePressed(){
		$this->deviceState--;
		echo "Channel Down \n";
	}

	public function buttonSixPressed(){
		$this->deviceState++;
		echo "Channel Up \n";
	}
}


abstract class RemoteButton
{
	private $theDevice;		// ot tip EntertainmentDevice

	public function __construct(EntertainmentDevice $newDevice){
		$this->theDevice = $newDevice;
	}

	public function buttonFivePressed(){
		$this->theDevice->buttonFivePressed();
	}
	public function buttonSixPressed(){
		$this->theDevice->buttonSixPressed();
	}

	public function deviceFeedback(){
		$this->theDevice->deviceFeedback();
	}

	abstract public function buttonNinePressed();
}


class TVRemoteMute extends RemoteButton
{
	public function __construct(EntertainmentDevice $newDevice){
		parent::__construct($newDevice);
	}

	public function buttonNinePressed(){
		echo "TV was Muted \n";
	}
}


class TVRemotePause extends RemoteButton
{
	public function __construct(EntertainmentDevice $newDevice){
		parent::__construct($newDevice);
	}

	public function buttonNinePressed(){
		echo "TV was Paused \n";
	}
}


$theTV = new TVRemoteMute(new TVDevice(1, 200));
$theTV2 = new TVRemotePause(new TVDevice(1, 200));

echo "Test TV with Mute \n";
$theTV->buttonFivePressed();
$theTV->buttonSixPressed();
$theTV->buttonNinePressed();

echo "Test TV with Pause \n";
$theTV2->buttonFivePressed();
$theTV2->buttonSixPressed();
$theTV2->buttonSixPressed();
$theTV2->buttonSixPressed();
$theTV2->buttonSixPressed();
$theTV2->buttonNinePressed();
$theTV2->deviceFeedback();
