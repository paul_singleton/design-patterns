<?php
/**
 * Подходящ когато трябва да се създава голям брой подобни обекти. И тогава вместо да ги съдаваш 
 * тези много обекти, по добре използвай вече създадените и само им задавай това, което е специфично 
 * за случая.
 *
 * В този пример имаш един прост клас Book, където имаш само автор и заглавие на книгата.
 *
 * Имаш един клас BookFactory, където имаш един масив от книги (private пропърти $books 
 * пълен с обекти на Book),
 * който масив го пълниш с метода getBook(),
 * на който подаваш номер на книга, и този номер ще е индекs в масива с книги.
 *
 * В getBook() първо проверяваш дали има вече такъв обект-книга (с такъв индекс), и ако да, 
 * направо го връщаш.
 *
 * Третия клас е BookShelf, където нищо не създаваш, само му подаваш книги-обекти през метода addBook() 
 * и пълниш в еднo private пропърти-масив $books
 * И можеш и да го изциклиш този масив за да покажеш всички книги (в метода showBooks())
 */

declare(strict_types = 1);

class Book
{
	private string $author, $title;

	public function __construct(string $author_in = '', string $title_in = '')
	{
		$this->author = $author_in;
		$this->title  = $title_in;
	}

	public function getAuthor() : string
	{
		return $this->author;
	}

	public function getTitle() : string
	{
		return $this->title;
	}
}


class BookFactory
{
	private array $books = array();

	public function __construct()
	{
		$this->books[1] = NULL;
		$this->books[2] = NULL;
		$this->books[3] = NULL;
	}

	public function getBook(int $iBookKey) : Book
	{
		if (NULL == $this->books[$iBookKey]) {
			$makeFunction = 'makeBook' . $iBookKey;
			$this->books[$iBookKey] = $this->$makeFunction();
		}
		return $this->books[$iBookKey];
	}

	// Sort of an long way to do this, but hopefully easy to follow.
	// How you really want to make flyweights would depend on what your application needs.
	// This, while a little clumsy looking, does work well.
	private function makeBook1() : Book
	{
		return new Book('Larry Truett', 'PHP For Cats');
	}

	private function makeBook2() : Book
	{
		return new Book('Larry Truett', 'PHP For Dogs');
	}

	private function makeBook3() : Book
	{
		return new Book('Larry Truett', 'PHP For Parakeets');
	}
}


class BookShelf
{
	private array $books = array();

	public function addBook(Book $oBook) : void
	{
		$this->books[] = $oBook;
	}

	public function showBooks() : string
	{
		$str = '';
		foreach ($this->books as $oBook) {
			$str .= 'Zaglavie: ' . $oBook->getAuthor() . ' Avtor: ' . $oBook->getTitle() . '      ';
		}
		return $str;
	}
}


$bookFactory = new BookFactory();
$bookShelf1 = new BookShelf();

$book1 = $bookFactory->getBook(1);
$bookShelf1->addBook($book1);

$book2 = $bookFactory->getBook(1);
$bookShelf1->addBook($book2);

echo "Test 1 - show the two books are the same book \n";
if($book1 === $book2){
	echo '1 and 2 are THE SAME, te sa i dvata: ' . $book1->getAuthor() . ' - ' . $book1->getTitle();
} else {
	echo '1 and 2 are NOT the same';
}

echo "\n\n\n";

echo "Test 2 - with one book on one self twice \n";
echo $bookShelf1->showBooks();
echo "\n\n";

$bookShelf2 = new BookShelf();
$book1 = $bookFactory->getBook(2);
$bookShelf2->addBook($book1);
$bookShelf1->addBook($book1);

echo "Test 3 - book shelf one \n";
echo $bookShelf1->showBooks();
echo "\n\n";

echo "Test 4 - book shelf two \n";
echo $bookShelf2->showBooks();
echo "\n";
