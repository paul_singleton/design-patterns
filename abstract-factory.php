<?php
/**
"Provide an interface for creating families of related or dependent objects without specifying their concrete classes." (Gang Of Four)

Да се върнем към това кои бяха трите типа Фактори ДП-та : Simple Factory DP, Factory Method DP и Abstract Factory DP.
  1) Simple Factory е толкова прост, че не го броят даже за ДП. Имаш функция или клас PizzaFactory с метод makePizza($pizzaName),
  на който метод подаваш името на пицата и то ти създава и връща обектът от това име с new, като може и да има някаква логика преди да я създаде...
  Толкоз!

  2) Fctory Method DP - тук имаме повече от едно такова Фактори, които обаче наследяват един абсклас/интерфейс и следователно имат обща структура.
  И пак, на всяко от тях подаваш името на класа, то връща съответният обект, и може и някаква специфична логика да има около самото създаване на обекта.

  3) Abstract Factory DP - тук е нещо подобно, с разликата, че Factory-то няма да създава един обект от клас,
  а повече от един обекти, свързани смислово помежду си. Абстакт факторито е просто казано един абсклас или интерфейс, който задава какво всяка
  наследяваща фабрика трябва да има като методи.

Разликата между Factory Method DP и Abstract Factory DP e, че Abstract Factory DP връща много обекти, не само един като Factory Method DP.
И идеята е тези "много обекти" да са свързани логически помежду си (виж примера с колите по-долу).
The point is that you can group different concretions within cpecific concrete factory such that this group of concretions to make sence together.
And that the thig you can not do with the Factory Method DP because in it you simply have single factory method.
 
Имаш не просто една фабрика за обекти, а комбинат от фабрики, всяка от която прави обект, свързан логически с останалите.
Както например един комбинат за автомобили има отделни фабрики или цехове за отделните части, които са съставни за целия автомобил.
Имаш един абсклас или интерфейс Avtomobil - това ти е абстрактната фабрика (комбинат), който задава какви фабрики трябва да има всеки комбинат за автомобили - фабрика за гуми, фабрика за двигатели...  нея трябва да я наследяват/имплементират класове като
Audi, Opel, Merceres... Всеки комбинат като Audi напр. трябва да има фабрика за гуми, фабрика за двигатели...
Демек, Avtomobil задължава Audi, Opel, Merceres... да имат фабрики за гуми, двигатели...
*/



// От тук...
interface iAvtomobil
{
	public function makeTyres() : iTyres;
	public function makeEngine() : iEngines;
}

class Audi implements iAvtomobil
{
	public function makeTyres() : Goodyear
	{
		return new Goodyear;
	}
	public function makeEngine() : Dvigatel2
	{
		return new Dvigatel2;
	}
}

class Mercedes implements iAvtomobil
{
	public function makeTyres() : Bridgestone
	{
		return new Bridgestone;
	}
	public function makeEngine() : Dvigatel1
	{
		return new Dvigatel1;
	}
}
// ... до тук, задаваш отделните "фабрики за конкретни марки", всяка от които трябва да екстендва/имплементира един абсклас/интерфейс



// И от тук ...
interface iTyres
{
	public function getRubberType();
	public function getWidth();
}

class Goodyear implements iTyres
{
	public function getRubberType(){
		...
	}
	public function getWidth(){
		...
	}
}

class Bridgestone implements iTyres
{
	public function getRubberType(){
		...
	}
	public function getWidth(){
		...
	}
}


interface iEngines
{
	public function getPower();
	public function getFuelConsumption();
}

class Dvigatel1 implements iEngines
{
	public function getPower(){
		...
	}
	public function getFuelConsumption(){
		...
	}
}

class Dvigatel2 implements iEngines
{
	public function getPower(){
		...
	}
	public function getFuelConsumption(){
		...
	}
}
// ... до тук, задаваш продуктите, които всяка от горните "фабрики" ще произвежда



/**
Фактически, за да имаше Abstract Factory, трябва да си създадеш отделни фабрики, които екстендват/имплементират общ абсклас/интерф. 
(Фабрики А) и отделно трябва да имаш една или повече фабрики за продукти, които пак са на принципа "родителски абсклас/интерф" 
и наследяващи го фабрики. (Фабрики Б)

И също, във всяка от Фабрики А трябва да зададеш от кой продукт от Фабрики Б ще се произвежда.

Пример: искаш да произвеждаш Ауди-та, или Мерцедес-и. Правиш си един абсклас/интерф. Avtomobili който Ауди, Мерцедес... да наследяват/имплементират.
Ето ти Фабрики А - всяка от тези, била тя Ауди или мерцедес или...
Обаче всяко Ауди или Мерцедес трябва задължително да има: гуми, двигател, спирачки...
Правиш си абсклас/интерф. Tyres и му правишш отделните подкласоове за отделните марки гуми.
За Двигатели - подобно, за спирачки - подобно...
Ето ти Фабрики Б

И накрая трябва да зададеш във всяка от Фабрики А коя от Фабрики Б ти трябва, демек, Ауди ще е с гуми Бриджстойн,
двигател едикъв си, спирачки едикви си...
Мерцедес ще е с гуми Гудиър, двигате едикъв си...

"Provide an interface for creating families of related or dependent objects without specifying their concrete classes." (Gang Of Four)
"creating families of related or dependent objects" ???
Kaкви са тези семейства от свързани обекти? Нали като създаваш обект Ауди например, ти създаваш и обекти от клас Tyres, Engine, Brakes...
те са свързани, нали съвкупността от гуми, двигател, спирачки... правят едно Ауди, или едим Мерцедес.
"without specifying their concrete classes" e няма как да създадеш обект без да зададеш от кой клас дае, но тук явно се има предвид
да зададеш класа вътре в конкретното Фактори А, демек всяко Ауди какви гуми, двигател, спирачки и т.н. да има. Но не и в родитеслкият абсклас/интерфейс.
В родителския абсклас/интерф само задаваш, че всяко Фактори А трябва да има гуми, двигател, спирачки... Какви точно, то си решава.

И изобщо, защо Abstract Faktory, кое е това абстрактно фактори, дето произвеждало много свързани обекти?
Ми Ауди е едно такова Абстрактно Фактори, Мерцедес е друго такова. е произвеждат много обекти - от клас Гъми, от клас Двигатели, от клас Спирачки...
*/



echo "\n\n\n**************************************************\n\n\n";



interface Animal
{
	public function sound();
}

class Lion implements Animal
{
	public function sound(){}
}

class Hiena implements Animal
{
	public function sound(){}
}

class Elephant implements Animal
{
	public function sound(){}
}


interface ZooparkFactory  /* може и да е абсклас */
{
	public function animal1() : Animal;
	public function animal2() : Animal;
	// ...
}

class SofiaZoopark implements ZooparkFactory
{
	public function animal1() : Animal {
		return new Hiena();
	}
	public function animal2() : Animal {
		return new Lion();
	}
	// ...
}

class BerlinZoopark implements ZooparkFactory
{
	public function animal1() : Animal {
		return new Lion();
	}
	public function animal2() : Animal {
		return new Elephant();
	}
	// ...
}



echo "\n\n\n**************************************************\n\n\n";



abstract class AbstractBookFactory
{
	abstract public function makePHPBook();
	abstract public function makeMySQLBook();
}

class OReillyBookFactory extends AbstractBookFactory
{
	private $context = 'OReilly';

	public function makePHPBook(){
		return new OReillyPHPBook;
	}
	public function makeMySQLBook(){
		return new OReillyMySQLBook;
	}
}

class SamsBookFactory extends AbstractBookFactory
{
	private $context = 'Sams';

	public function makePHPBook(){
		return new SamsPHPBook;
	}
	public function makeMySQLBook(){
		return new SamsMySQLBook;
	}
}



abstract class AbstractMySQLBook
{
	private $subject = 'MySQL';
}

class OReillyMySQLBook extends AbstractMySQLBook
{
	private $author, $title;

	public function __construct(){
		$this->author = 'Tim King';
		$this->title  = 'Managing and Using MySQL';
	}
	public function getAuthor(){
		return $this->author;
	}
	public function getTitle(){
		return $this->title;
	}
}

class SamsMySQLBook extends AbstractMySQLBook
{
	private $author, $title;

	public function __construct(){
		$this->author = 'Paul Dubois';
		$this->title  = 'MySQL, 3rd Edition';
	}
	public function getAuthor(){
		return $this->author;
	}
	public function getTitle(){
		return $this->title;
	}
}



abstract class AbstractPHPBook
{
	private $subject = 'PHP';
}

class OReillyPHPBook extends AbstractPHPBook
{
	private $author, $title;
	private static $oddOrEven = 'odd';

	public function __construct(){
		// alternate between 2 books
		if('odd' == self::$oddOrEven){
			$this->author = 'Rasmus Lerdorf and Kevin Tatroe';
			$this->title  = 'Programming PHP';
			self::$oddOrEven = 'even';
		} else {
			$this->author = 'David Sklar and Adam Trachtenberg';
			$this->title  = 'PHP Cookbook';
			self::$oddOrEven = 'odd';
		}
	}
	public function getAuthor(){
		return $this->author;
	}
	public function getTitle(){
		return $this->title;
	}
}

class SamsPHPBook extends AbstractPHPBook
{
	private $author, $title;

	public function __construct(){
		// alternate randomly between 2 books
		mt_srand((double)microtime() * 10000000);
		$rand_num = mt_rand(0, 1);

		if($rand_num < 1){
			$this->author = 'George Schlossnagle';
			$this->title  = 'Advanced PHP Programming';
		} else {
			$this->author = 'Christian Wenz';
			$this->title  = 'PHP Phrasebook';
		}
	}
	public function getAuthor(){
		return $this->author;
	}
	public function getTitle(){
		return $this->title;
	}
}


echo "Testing OReillyBookFactory \n";
$bookFactoryInstance = new OReillyBookFactory;
testConcreteFactory($bookFactoryInstance);

echo "\n";

echo "Testing SamsBookFactory \n";
$bookFactoryInstance = new SamsBookFactory;
testConcreteFactory($bookFactoryInstance);

function testConcreteFactory($bookFactoryInstance){
	$phpBookOne = $bookFactoryInstance->makePHPBook();
	echo 'First php Author: ' . $phpBookOne->getAuthor() . "\n";
	echo 'First php Title: ' . $phpBookOne->getTitle() . "\n";

	$phpBookTwo = $bookFactoryInstance->makePHPBook();
	echo 'Second php Author: ' . $phpBookTwo->getAuthor() . "\n";
	echo 'Second php Title: ' . $phpBookTwo->getTitle() . "\n";

	$mySqlBook = $bookFactoryInstance->makeMySQLBook();
	echo 'MySQL Author: ' . $mySqlBook->getAuthor() . "\n";
	echo 'MySQL Title: ' . $mySqlBook->getTitle() . "\n";
}



echo "\n\n\n**************************************************\n\n\n";



// Common interface
interface IInv {
	public function getGun();
	public function getRifle();
}
interface IRifle {}
interface IGun {}

// USSR - Red army
class AK47 implements IRifle
{
	public function __construct(){
		echo "Bum bum AK47 \n";
	}
}
class Makarov implements IGun
{
	public function __construct(){
		echo "Bum bum Makarov \n";
	}
}
class USSRInventory implements IInv
{
	public function getGun(){
		return new Makarov();
	}
	public function getRifle(){
		return new AK47();
	}
}

// USA - US Army
class M16 implements IRifle
{
	public function __construct(){
		echo "Bum bum M16 \n";
	}
}
class Beretta implements IGun
{
	public function __construct(){
		echo "Bum bum Beretta \n";
	}
}
class USAInventory implements IInv
{
	public function getGun(){
		return new Beretta();
	}
	public function getRifle(){
		return new M16();
	}
}

$country = 'USSR';
if($country === 'USSR'){
	$afact = new USSRInventory();
} elseif($country === 'USA'){
	$afact = new USAInventory();
}
var_dump($afact->getGun());



echo "\n\n\n**************************************************\n\n\n";



// This acts as an ordering mechanism for creating EnemyShips that have a weapon, engine & name
// & nothing else
abstract class EnemyShipBuilding
{
	// The specific parts used for engine & weapon depend upon the string that is passed to this method
	abstract protected function makeEnemyShip($typeOfShip = '');

	// When called a new EnemyShip is made. The specific parts are based on the String entered.
	// After the ship is made we execute multiple methods in the EnemyShip Object
	public function orderTheShip($typeOfShip = ''){
		$theEnemyShip = $this->makeEnemyShip($typeOfShip);
		$theEnemyShip->makeShip();
		$theEnemyShip->displayEnemyShip();
		$theEnemyShip->followHeroShip();
		$theEnemyShip->enemyShipShoots();
		return $theEnemyShip;
	}
}


// This is the only class that needs to change, if you want to determine which enemy ships you want to provide as an option to build
class UFOEnemyShipBuilding extends EnemyShipBuilding
{
	protected function makeEnemyShip($typeOfShip = ''){
		$theEnemyShip = null;

		// If UFO was sent grab use the factory that knows what types of weapons and engines a regular UFO  needs.
		// The EnemyShip object is returned & given a name
		if($typeOfShip === 'UFO'){
			$shipPartsFactory = new UFOEnemyShipFactory();
			$theEnemyShip = new UFOEnemyShip($shipPartsFactory);
			$theEnemyShip->setName('UFO Grunt Ship');
		} else {
			// If UFO BOSS was sent grab use the factory that knows what types of weapons and engines a Boss UFO needs.
			// The EnemyShip object is returned & given a name
			if($typeOfShip === 'UFO BOSS'){
				$shipPartsFactory = new UFOBossEnemyShipFactory();
				$theEnemyShip = new UFOBossEnemyShip($shipPartsFactory);
				$theEnemyShip->setName('UFO Boss Ship');
			}
		}
		return $theEnemyShip;
	}
}


// With an Abstract Factory Pattern you won't just build ships, but also all of the components for the ships
// Here is where you define the parts that are required if an object wants to be an enemy ship
interface EnemyShipFactory
{
	public function addESGun();
	public function addESEngine();
}

// This factory uses the EnemyShipFactory interface to create very specific UFO Enemy Ship
// This is where we define all of the parts the ship will use by defining the methods implemented being ESWeapon and ESEngine
// The returned object specifies a specific weapon & engine
class UFOEnemyShipFactory implements EnemyShipFactory
{
	// Defines the weapon object to associate with the ship
	public function addESGun(){
		return new ESUFOGun(); // Specific to regular UFO
	}

	// Defines the engine object to associate with the ship
	public function addESEngine(){
		return new ESUFOEngine(); // Specific to regular UFO
	}
}

// This factory uses the EnemyShipFactory interface to create very specific UFO Enemy Ship
// This is where we define all of the parts the ship will use by defining the methods implemented being ESWeapon and ESEngine
// The returned object specifies a specific weapon & engine
class UFOBossEnemyShipFactory implements EnemyShipFactory
{
	// Defines the weapon object to associate with the ship
	public function addESGun(){
		return new ESUFOBossGun(); // Specific to Boss UFO
	}

	// Defines the engine object to associate with the ship
	public function addESEngine(){
		return new ESUFOBossEngine(); // Specific to Boss UFO
	}
}


abstract class EnemyShip
{
	private $name = '';

	// Newly defined objects that represent weapon & engine
	// These can be changed easily by assigning new parts in UFOEnemyShipFactory or UFOBossEnemyShipFactory
	/*ESWeapon*/ protected $weapon;
	/*ESEngine*/ protected $engine;

	public function getName(){
		return $this->name;
	}
	public function setName($newName = ''){
		$this->name = $newName;
	}

	abstract protected function makeShip();

	// Because I defined the toString method in engine
	// when it is printed the String defined in toString goes on the screen
	public function followHeroShip(){
		echo $this->getName() . " is following the hero at " . $this->engine . "\n";
	}
	public function displayEnemyShip(){
		echo $this->getName() . " is on the screen \n";
	}
	public function enemyShipShoots(){
		echo $this->getName() . " attacks and does " . $this->weapon . "\n";
	}

	// If any EnemyShip object is printed to screen this shows up
	public function __toString(){
		return 'The ' . $this->name . ' has a top speed of ' . $this->engine . ' and an attack power of ' . $this->weapon;
	}
}

class UFOEnemyShip extends EnemyShip
{
	// We define the type of ship we want to create by stating we want to use the factory that makes enemy ships
	/*EnemyShipFactory*/ private $shipFactory;

	// The enemy ship required parts list is sent to this method. They state that the enemy ship must have a weapon and engine assigned.
	// That object also states the specific parts needed to make a regular UFO versus a Boss UFO
	public function UFOEnemyShip(/*EnemyShipFactory*/ $shipFactory){
		$this->shipFactory = $shipFactory;
	}

	// EnemyShipBuilding calls this method to build a specific UFOEnemyShip
	public function makeShip(){
		echo 'Making enemy ship ' + $this->getName() + "\n";

		// The specific weapon & engine needed were passed in shipFactory. We are assigning those specific part objects to the UFOEnemyShip here
		$this->weapon = $this->shipFactory->addESGun();
		$this->engine = $this->shipFactory->addESEngine();
	}
}

class UFOBossEnemyShip extends EnemyShip
{
	// We define the type of ship we want to create by stating we want to use the factory that makes enemy ships
	/*EnemyShipFactory*/ protected $shipFactory;

	// The enemy ship required parts list is sent to this method. They state that the enemy ship must have a weapon and engine assigned.
	// That object also states the specific parts needed to make a Boss UFO versus a Regular UFO
	public function UFOBossEnemyShip(/*EnemyShipFactory*/ $shipFactory){
		$this->shipFactory = $shipFactory;
	}

	// EnemyShipBuilding calls this method to build a specific UFOBossEnemyShip
	public function makeShip(){
		// TODO Auto-generated method stub
		echo 'Making enemy ship ' . $this->getName() . "\n";

		// The specific weapon & engine needed were passed in shipFactory.
		// We are assigning those specific part objects to the UFOBossEnemyShip here
		$this->weapon = $this->shipFactory->addESGun();
		$this->engine = $this->shipFactory->addESEngine();
	}
}

// Any part that implements the interface ESEngine can replace that part in any ship
interface ESEngine {
	// User is forced to implement this method. It outputs the string returned when the object is printed
	public function __toString();
}

// Any part that implements the interface ESWeapon can replace that part in any ship
interface ESWeapon {
	// User is forced to implement this method
	// It outputs the string returned when the object is printed
	public function __toString();
}

// Here we define a basic component of a space ship
// Any part that implements the interface ESWeapon can replace that part in any ship
class ESUFOGun implements ESWeapon
{
	// EnemyShip contains a reference to the object ESWeapon. It is stored in the field weapon
	// The Strategy design pattern is being used here
	// When the field that is of type ESUFOGun is printed the following shows on the screen
	public function __toString(){
		return '20 damage';
	}
}

// Here we define a basic component of a space ship
// Any part that implements the interface ESEngine can replace that part in any ship
class ESUFOEngine implements ESEngine
{
	// EnemyShip contains a reference to the object ESWeapon. It is stored in the field weapon
	// The Strategy design pattern is being used here
	// When the field that is of type ESUFOGun is printed the following shows on the screen
	public function __toString(){
		return '1000 mph';
	}
}

// Here we define a basic component of a space ship
// Any part that implements the interface ESWeapon can replace that part in any ship
class ESUFOBossGun implements ESWeapon
{
	// EnemyShip contains a reference to the object ESWeapon. It is stored in the field weapon
	// The Strategy design pattern is being used here
	// When the field that is of type ESUFOGun is printed the following shows on the screen
	public function __toString(){
		return '40 damage';
	}
}

// Here we define a basic component of a space ship
// Any part that implements the interface ESEngine can replace that part in any ship
class ESUFOBossEngine implements ESEngine
{
	// EnemyShip contains a reference to the object ESWeapon. It is stored in the field weapon
	// The Strategy design pattern is being used here
	// When the field that is of type ESUFOGun is printed the following shows on the screen
	public function __toString(){
		return '2000 mph';
	}
}


// EnemyShipBuilding handles orders for new EnemyShips
// You send it a code using the orderTheShip method & it sends the order to the right factory for creation

/*EnemyShipBuilding*/ $MakeUFOs = new UFOEnemyShipBuilding();

/*EnemyShip*/ $theGrunt = $MakeUFOs->orderTheShip('UFO');
echo $theGrunt . "\n";

/*EnemyShip*/ $theBoss = $MakeUFOs->orderTheShip('UFO BOSS');
echo $theBoss . "\n";
