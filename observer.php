<?php
/**
 * Този Design Pattern е подходящ ако примерно искаш много обекти да обработят промяна в даден обект.
 * Примерно имаш обект Продукт, който има разни пропъртита (цена, наличност, описание...) и примерно трябва да се промени цената (да поскъпне).
 * И примерно трябва освен да UPDATE-неш таблицата в базата, трябва и да изпратиш емайл на всички клиенти за да има кажеш, че е поскъпнал.
 *
 * Има 2 вида обекти (от два класа) - Субект и Обзървър.
 * На всеки Субект му се задават 1 или повече Обзървъри (като масив) с метода attach(), а могат и да се махат с detach()
 * Важен е Субектът, него ще следим дали се променя. Но то не е някакво авоматично следене, просто ако примерно със сетър ми сетнеш накакво пропърти,
 * и ако искаш обзървърите му да "разберат", ги изцикляш с foreach() и за всеки обзървър му предаваш текущата инстанция с $this (себе си).
 * Това "предаване" на Субект на Обзървър става с метод на обзървъра (update()), който приема Субект-а ($this-a) и вече има копие на този Субект.
 * В долния пример update() просто показва пропъртито на Субект-а, което сме променили.
 *
 * Всеки Субект трябва да има 3 методи - attach(), detach() и notify()
 * attach() - подаваш му обект Обзървър, веднъж, дваж или колкото трябва и той (attach()) го добавя като елемент в масив-пропърти на Субекта.
 * detach() - когато искаш да махнеш някой Обзървър от Субекта, предаваш го като параметър, изцикляш всички (масива с обзървърите) и като го намериш го unset()-ваш
 * notify() - тук става самото "информиране" на всеки от Обзървър-ите за дадена промяна. Този метод ТРЯБВА да се извика за да стане това.
 * В него се изциклят всички обзървъри (масива) и на всеки му викаш метода update($this), на който предаваш дадения Субект.
 */


declare(strict_types = 1);


/**
 * Понеже може да имаме много обзървъри, нека ги задължим да имат метод update() за да го викаме унифицирано
 */
interface IObserver
{
	public function update(ISubject $oSubject) : void;
}

/**
 * Понеже можем да имаме много субекти (наблюдавани обекти), нека ги задължим да имат методи за:
 *		- добавяне на обзървър към даденият обект;
 *		- махане на обзървър от даденият обект;
 *		- оповестяване на обзървърите, закачени към субекта. Този метод трябва изрично да се извика при настъпване на дадено събитие.
 */
interface ISubject
{
	public function attach(IObserver $oObserver) : void;
	public function detach(IObserver $oObserver) : void;
	public function notify();
}


class FirstExampleObserver implements IObserver
{
	/**
	 * Когато в някой Субект настъпи някаква промяна, той (Субектът) ще вика този метод на всичките си обзървъри.
	 * Примерно като ги изцикля ако са в масив.
	 * И така образно казано ще им казва, че с нещо се е променил, да знаят. И трябра да им предава на всеки по едно $this
	 *
	 * В нашия случай този метод не прави нищо кой знае какво, просто показва промените.
	 */
	public function update(ISubject $oSubject) : void
	{
		echo "\n\n";
		echo "*IN PATTERN OBSERVER - NEW GOSSIP HAS ARRIVED*\n";
		echo $oSubject->getFavorites() . "\n";
		echo "\n\n";
	}
}


class FirstExampleSubject implements ISubject
{
	private string $favorites = '';
	private array $observers = array();		// Array with observers for the given "FirstExampleSubject" object

	/**
	 * The "subject" must maintain a list (array) of "observers" (or dependents) objects
	 * and notifies them when a changes in it (the subject) happens,
	 * usually by calling one of their methods (in this case - update()).
	 *
	 * Т.е. с attach() задаваш обзървърите на даден събджект.
	 */
	public function attach(IObserver $oObserver) : void
	{
		$this->observers[] = $oObserver;
	}

	/**
	 * Това е обратното на attach(), когато искаш да махнеш даден обзървър от събджекта.
	 * Предаваш като параметър обекта-обзървър, изцикляш на събджекта всички обзъръври,
	 * при всяка итерация проверяваш всеки дали не е този, който си предал (за махане), и го unset()-ваш ако да.
	 */
	public function detach(IObserver $oObserver) : void
	{
		foreach ($this->observers as $oKey => $oVal)
			if ($oVal == $oObserver)
				unset($this->observers[$oKey]);
	}

	/**
	 * Този метод се вика, когато искаш да "информираш" всички обзървъри на даден събджект.
	 * Той ги изцикля и за всеки вика update() като предава по едно $this
	 * Всеки обзървър тряба да има метод update(), което се гарантира с интерфейс.
	 */
	public function notify() : void
	{
		foreach ($this->observers as $observer)
			$observer->update($this);
	}

	/**
	 * Единият начин за "нотифайване" на обзървъра е
	 * като директо се извика notify() в даденият метод
	 */
	public function updateFavorites(string $fav = '') : void
	{
		$this->favorites = $fav;
		$this->notify();
	}

	/**
	 * Може и така, с "чейване на влакче".
	 */
	public function setFavorites(string $fav) : self
	{
		$this->favorites = $fav;
		return $this;
	}

	/**
	 * Може и да не нотифайваме ;-P
	 */
	public function getFavorites() : string
	{
		return $this->favorites;
	}
}


$kljukar1 = new FirstExampleSubject();

$patternGossipFan = new FirstExampleObserver();

$kljukar1->attach($patternGossipFan);

$kljukar1->updateFavorites('Pena se razvede, Ivan izneveri na jena si, Pe6o si nabi svekyrvata');
$kljukar1->updateFavorites('Kak Minka ne moje da gotvi, Stancho pie mnogo');
$kljukar1->setFavorites('Gosho krade nafta')->notify();		// Може и така, с "влакче".

$kljukar1->detach($patternGossipFan);

$kljukar1->updateFavorites('Tova veche ne go nabludavame, zashtoto detach-nahme observera');
