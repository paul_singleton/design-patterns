<?php
/**
 * Полезен е примерно за преобразувания на една единица мярка в друга, примерно валутни курсове, 
 * метри в инчове, галони в литри...
 * Used to convert one representation of data into another.
 *
 * Има 3 части (класа)
 * Context - съдържа информацията, която ще преобразуваме.
 * Expression - абстрактен клас, дефиниращ всички методи, използвани за преобразуване.
 * Terminal или Concrete Expressions, където са дефинирани методи за по-специфично преобразуване на
 * различните типове информация.
 * Обикновено в Context не са дефинирани никакви мотоди за преобразуване, а са само декларирани 
 * (може би вместо да е абстрактен клас, може да е интерфейс),
 * а са дефинирани в Terminal/Concrete Expression. Който (Terminal/Concrete Expression) трябва да
 * наследява/имплементира Expression.
 *
 * В долният пример имаме една един елементарен клас "Book", чиито обекти съдържат име и автор на дадена книга.
 * Имаме и клас "BookList", където в private масив $book пълним горните обекти на "Book" с метода "addBook()".
 * Имаш един клас Interpreter, където става всичко. Създаваш нов обект и му предаваш обектът на "BookList",
 * където са и обектите-книги (в private масив $book, но щом имаме гетър не е проблем, че е private).
 * Този обект на "BookList", който току що предадохме, го сетваме на private пропърти $bookList.
 * Там има и едни метод "interpret()", на който му се подава стринг като примерно "book title 2", с който 
 * му се казва да върне заглавието на втората книга.
 * Ако му подадем "book author title 1" ще върне автора и заглавието на първата книга.
 */

declare(strict_types = 1);

class Book
{
	private string $author, $title;

	public function __construct(string $title_in = '', string $author_in = '')
	{
		$this->author = $author_in;
		$this->title  = $title_in;
	}

	public function getAuthor() : string
	{
		return $this->author;
	}

	public function getTitle() : string
	{
		return $this->title;
	}

	public function getAuthorAndTitle() : string
	{
		return $this->getTitle() . ' by ' . $this->getAuthor();
	}
}


class Interpreter
{
	private BookList $bookList;

	public function __construct(BookList $bookListIn)
	{
		$this->bookList = $bookListIn;
	}

	public function interpret(string $stringIn) : string
	{
		$arrayIn = explode(' ', $stringIn);

		$returnString = NULL;

		// go through the array validating
		// and if possible calling a book method
		// could use refactoring, some duplicate logic
		if ('book' === $arrayIn[0]) {
			if ('author' === $arrayIn[1]) {
				if (is_numeric($arrayIn[2])) {
					$book = $this->bookList->getBook($arrayIn[2]);
					if (NULL === $book) {
						$returnString = 'Can not process, there is no book # ' . $arrayIn[2];
					} else {
						$returnString = $book->getAuthor();
					}
				} elseif ('title' === $arrayIn[2]) {
					if (is_numeric($arrayIn[3])) {
						$book = $this->bookList->getBook($arrayIn[3]);
						if (NULL === $book) {
							$returnString = 'Can not process, there is no book # ' . $arrayIn[3];
						} else {
							$returnString = $book->getAuthorAndTitle();
						}
					} else {
						$returnString = 'Can not process, book # must be numeric.';
					}
				} else {
					$returnString = 'Can not process, book # must be numeric.';
				}
			}

			if ('title' === $arrayIn[1]) {
				if (is_numeric($arrayIn[2])) {
					$book = $this->bookList->getBook($arrayIn[2]);
					if (NULL === $book) {
						$returnString = 'Can not process, there is no book # ' . $arrayIn[2];
					} else {
						$returnString = $book->getTitle();
					}
				} else {
					$returnString = 'Can not process, book # must be numeric.';
				}
			}
		} else {
			$returnString = 'Can not process, '.
							'can only process book author #, '.
							'book title #, or book author title #';
		}

		return $returnString;
	}
}


class BookList
{
	private array $books = array();	// Тук ще пълним обектите-книги
	private int $bookCount = 0;		// Това е само да следим бройката, то не е важно.

	public function getBookCount() : int
	{
		return $this->bookCount;
	}

	private function setBookCount(int $newCount) : void
	{
		$this->bookCount = $newCount;
	}

	public function getBook(string $bookNumberToGet) : ?Book
	{
		if (is_numeric($bookNumberToGet) && ($bookNumberToGet <= $this->getBookCount())) {
			return $this->books[$bookNumberToGet];
		}
		return NULL;
	}

	public function addBook(Book $book_in) : int
	{
		$this->setBookCount($this->getBookCount() + 1);
		$this->books[$this->getBookCount()] = $book_in;
		return $this->getBookCount();
	}

	public function removeBook(Book $book_in) : int
	{
		$counter = 0;
		while (++$counter <= $this->getBookCount()) {
			if ($book_in->getAuthorAndTitle() === $this->books[$counter]->getAuthorAndTitle()) {
				for ($x = $counter; $x < $this->getBookCount(); $x++) {
					$this->books[$x] = $this->books[$x + 1];
				}
				$this->setBookCount($this->getBookCount() - 1);
			}
		}
		return $this->getBookCount();
	}
}





// load BookList for test data
$bookList = new BookList();
$bookList->addBook(new Book('PHP for Cats', 'Larry Truett'));
$bookList->addBook(new Book('MySQL for Dogs', 'Ivan Stanchev'));

$interpreter = new Interpreter($bookList);

echo 'Test 1 - Gre6ka: formatyt na requesta trqbwa da e "book author #", "book title #" ili "book author title #"' . "\n";
echo $interpreter->interpret('author 1');
echo "\n\n";

echo 'Test 2 - Pokazva avtoryt na vtorata kniga' . "\n";
echo $interpreter->interpret('book author 2');
echo "\n\n";

echo 'Test 3 - Pokazva zaglavieto na vtorata kniga' . "\n";
echo $interpreter->interpret('book title 2');
echo "\n\n";

echo 'Test 4 - Pokazva avtoryt i zaglavieto na pyrvata kniga' . "\n";
echo $interpreter->interpret('book author title 1');
echo "\n\n";

echo 'Test 5 - Gre6ka: nqma kniga s nomer 3' . "\n";
echo $interpreter->interpret('book title 3');
echo "\n\n";

echo 'Test 6 - Gre6ka: "one" e nevaliden nomer na kniga, trqbwa da e 4islo' . "\n";
echo $interpreter->interpret('book title one');
echo "\n\n";
