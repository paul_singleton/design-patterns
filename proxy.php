<?php
/**
 * "Provide a surrogate or placeholder for another object to control access to it." (Gang Of Four)
 *
 * Подходящ е например когато трябва да създаваш много обекти, които са големи, и затова е добре да ги създаваш само по веднъж.
 * За това има техника наречена "Lazy initialization", при която използваш нещо като "object pool" примерно масив, 
 * където първо проверяваш дали вече го няма този обект, ако да - го връщаш, ако не - с "new" го създаваш, 
 * добавяш го, и пак го връщаш.
 * Така, следващият път като ти трябва обект от този клас, направо го връщаш, защото той ще е като елемент от този масив.
 * В този смисъл е доста близък до Сингълтън.
 *
 * Имаш даден клас, от който трябва да създадеш обект и да извикаш метод на този обект (стандартна ситуация).
 * Естествено можеш да го направиш по директният начин.
 *
 * Но можеш и да си напишеш още един клас, който ще е т.н. прокси, и в него ще дефинираш един метод 
 * със същото име като това на метода от оригиналния клас.
 * И там ще създаваш обект от оригиналния клас (ако вече не е бил създаден) и с него ще викаш оригиналния метод.
 * Естествено, това може да го направиш и за други методи от оригиналния клас.
 * Цялата работа е, да не създаваш директно обекти и да им викаш методи, а да имаш класове-посредници 
 * (за всеки по един). Така можеш примерно да си правиш разни проверки преди да създадеш нов обект.
 * In short, the proxy is the object that is being called by the client to access the real object behind the scene.
 * Подобно на прокси-сървърите, които връщат контент, който имат подобно на кеш, без да занимават реалния сървър.
 *
 * Биват: Remote, Virtual и Protection.
 */

class ProxyBookList
{
	private ?BookList $bookList = NULL;

	public function getBookCount()
	{
		if (NULL == $this->bookList) {
			$this->makeBookList();
		}
		return $this->bookList->getBookCount();
	}

	public function addBook(Book $oBook) : int
	{
		if (NULL == $this->bookList) {
			$this->makeBookList();
		}
		return $this->bookList->addBook($oBook);
	}

	public function getBook(int $bookNum) : Book
	{
		if (NULL == $this->bookList) {
			$this->makeBookList();
		}
		return $this->bookList->getBook($bookNum);
	}

	public function removeBook($oBook){
		if(NULL == $this->bookList){
			$this->makeBookList();
		}
		return $this->bookList->removeBook($oBook);
	}

	// Create
	public function makeBookList(){
		$this->bookList = new BookList();
	}
}


class BookList
{
	private array $books = array();
	private int $bookCount = 0;

	public function getBookCount() : int
	{
		return $this->bookCount;
	}

	private function setBookCount(int $newCount) : void
	{
		$this->bookCount = $newCount;
	}

	public function getBook(int $bookNumberToGet) : ?Book
	{
		if(is_numeric($bookNumberToGet) && ($bookNumberToGet <= $this->getBookCount())){
			return $this->books[$bookNumberToGet];
		}
		return NULL;
	}

	public function addBook(Book $book_in) : int
	{
		$this->setBookCount($this->getBookCount() + 1);
		$this->books[$this->getBookCount()] = $book_in;
		return $this->getBookCount();
	}

	public function removeBook(Book $book_in) : int
	{
		$counter = 0;

		while (++$counter <= $this->getBookCount()) {
			if ($book_in->getAuthorAndTitle() == $this->books[$counter]->getAuthorAndTitle()) {
				for ($x = $counter; $x < $this->getBookCount(); $x++) {
					$this->books[$x] = $this->books[$x + 1];
				}
				$this->setBookCount($this->getBookCount() - 1);
			}
		}

		return $this->getBookCount();
	}
}


class Book
{
	private string $author, $title;

	public function __construct(string $title_in, string $author_in)
	{
		$this->author = $author_in;
		$this->title  = $title_in;
	}

	private function getAuthor() : string
	{
		return $this->author;
	}

	private function getTitle() : string
	{
		return $this->title;
	}

	public function getAuthorAndTitle() : string
	{
		return $this->getTitle() . ' by ' . $this->getAuthor();
	}
}


$book = new Book('PHP for Cats', 'Larry Truett');

$proxyBookList = new ProxyBookList();

$proxyBookList->addBook($book);

echo "Test 1 - show the book count after a book is added \n";
echo $proxyBookList->getBookCount();
echo "\n\n";

echo "Test 2 - show the book \n";
$outBook = $proxyBookList->getBook(1);
echo $outBook->getAuthorAndTitle();
echo "\n\n";

$proxyBookList->removeBook($outBook);

echo "Test 3 - show the book count after a book is removed \n";
echo $proxyBookList->getBookCount();





echo "\n\n\n**************************************************\n\n\n";





interface IImage {
	public function display();
}

class RealImage implements IImage
{
	private $fileName;

	public function __construct($fileName = ''){
		$this->fileName = $fileName;
		echo 'Loading "' . $this->fileName . '"' . "\n";
	}

	public function display(){
		echo 'Displaying "' . $this->fileName . '"' . "\n";
	}
}

class ProxyImage implements IImage
{
	private $realImage = NULL;
	private $fileName;

	public function __construct($fileName = ''){
		$this->fileName = $fileName;
	}

	// Override
	// Забележи, че тук не правим нищо, освен да създадем обект на RealImage и да го сложим в private пропърти $realImage. И от него,
	// викаме истинският метод display()
	public function display(){
		if($this->realImage == NULL){
			$this->realImage = new RealImage($this->fileName);
		}
		$this->realImage->display();
	}
}

// Всичко е заради класа RealImage и неговият метод display()
// Мога директно да си създам обект от него и да си извикам метода display() ...
$realImage = new RealImage('some_filename.xzw');
$realImage->display();

echo "\n\n";

// ... но мога и да създам друг клас, който е фактически проксито, където да има пак такъв метод display(), който ще е посредник,
// ще вика истинският display() на класа RealImage. Там ще създадем обект на RealImage (ако вече няма такъв сетнат като private пропърти на ProxyImage),
// и с него ще извикаме истинският метод display()
$ProxyImage = new ProxyImage('other_filename.sck');
$ProxyImage->display();

// Трикатлъка тук е, че първо можеш да "прекараш" викането на метод на клас, да минава през друг метод на клас,
// а по този начин може да си направиш един "objects pool", прилагайки хватката "Lazy initialization" и да създаваш нов обект
// само ако преди това не е бил създаден.

// Може и интерфейс да имплементират двата класа - реалния и проксито, но това е само за прегледност.
