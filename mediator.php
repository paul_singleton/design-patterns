<?php
/**
 * Използва се да управлява комуникацията между различни близки обекти, т.н. "колеги", които нямат нужда да знаят нищ един за друг.
 * Например самолетите над летище, всички комуникират с контролната кула, която ги направлява, но никой от тях не знае нищо за останалите самолети.
 *
 * Трябва да имамаме един интерфейс IMediator и един абстрактен клас Colleague, който съдържа обекта-медиатор като private пропърти.
 * И интерфейса, и абстрактния клас трябва да имат общи методи, с които да става самата комуникация между "колегите".
 * Комуникацията ще се състои в това, накой "колега" да изпращат съобщение до медиатора, като той го изпраща до всеки от останалите.
 * Интерфейса IMediator има клас, който го имплементира и обектът от този клас е всъщност нашият медиатор.
 * Абстрактният клас пък служи за създаването на N на брой класове-колеги.
 * Всяка комуникация става в IMediator-а, а отделните Колеги не знаят нищо един за друг.
 *
 * В Медиатора трябва някак да имаш достъп до всички Колеги, приемрно да са в масив.
 * В абстрактния клас на Колегите трябва да имаш зададен Медиаторът - като private пропърти с обекта на Медиатора.
 * Което значи, че и всички Колеги ще го имат понеже наследяват този абстрактен клас (не директно, защото е private, но чрез "гетър")
 */

interface IMediator {
	public function change(BookColleague $changingClassIn);
}

class BookMediator implements IMediator
{
	private $authorObject, $titleObject;

	public function __construct($sAuthor = '', $sTitle = ''){
		$this->authorObject = new BookAuthorColleague($sAuthor, $this);
		$this->titleObject  = new BookTitleColleague($sTitle, $this);
	}
	public function getAuthor(){
		return $this->authorObject;
	}
	public function getTitle(){
		return $this->titleObject;
	}

	// When title or author change case, this makes sure the other stays in sync
	public function change(BookColleague $changingClassIn){
		if($changingClassIn instanceof BookAuthorColleague){
			if('upper' == $changingClassIn->getState()){
				if('upper' != $this->getTitle()->getState()){
					$this->getTitle()->setTitleUpperCase();
				}
			} elseif('lower' == $changingClassIn->getState()){
				if('lower' != $this->getTitle()->getState()){
					$this->getTitle()->setTitleLowerCase();
				}
			}
		} elseif($changingClassIn instanceof BookTitleColleague){
			if('upper' == $changingClassIn->getState()){
				if('upper' != $this->getAuthor()->getState()){
					$this->getAuthor()->setAuthorUpperCase();
				}
			} elseif('lower' == $changingClassIn->getState()){
				if('lower' != $this->getAuthor()->getState()){
					$this->getAuthor()->setAuthorLowerCase();
				}
			}
		}
	}
}


abstract class BookColleague
{
	private $mediator;

	public function __construct($oMediator = NULL){
		$this->mediator = $oMediator;
	}
	public function getMediator(){
		return $this->mediator;
	}
}


class BookAuthorColleague extends BookColleague
{
	private $author, $state;

	public function __construct($sAuthor = '', $oMediator = NULL){
		$this->author = $sAuthor;
		parent::__construct($oMediator);
	}

	// Author and Title "setters" and "getters" BEGIN
	public function getAuthor(){
		return $this->author;
	}
	public function setAuthor($sAuthor = ''){
		$this->author = $sAuthor;
	}

	public function getState(){
		return $this->state;
	}
	public function setState($sState = ''){
		$this->state = $sState;
	}
	// Author and Title "setters" and "getters" END

	public function setAuthorUpperCase(){
		$this->setAuthor(strtoupper($this->getAuthor()));
		$this->setState('upper');
		$this->getMediator()->change($this);
	}

	public function setAuthorLowerCase(){
		$this->setAuthor(strtolower($this->getAuthor()));
		$this->setState('lower');
		$this->getMediator()->change($this);
	}
}


class BookTitleColleague extends BookColleague
{
	private $title, $state;

	public function __construct($sTitle = '', $oMediator = NULL){
		$this->title = $sTitle;
		parent::__construct($oMediator);
	}

	// Author and Title "setters" and "getters" BEGIN
	public function getTitle(){
		return $this->title;
	}
	public function setTitle($sTitle = ''){
		$this->title = $sTitle;
	}

	public function getState(){
		return $this->state;
	}
	public function setState($sState = ''){
		$this->state = $sState;
	}
	// Author and Title "setters" and "getters" END

	public function setTitleUpperCase(){
		$this->setTitle(strtoupper($this->getTitle()));
		$this->setState('upper');
		$this->getMediator()->change($this);
	}

	public function setTitleLowerCase(){
		$this->setTitle(strtolower($this->getTitle()));
		$this->setState('lower');
		$this->getMediator()->change($this);
	}
}


$mediator = new BookMediator('Evelyn Waugh', 'Brideshead Revisited');
$oAuthor = $mediator->getAuthor();
$oTitle = $mediator->getTitle();
echo "Original Author and Title: \n";
echo 'автор: ' . $oAuthor->getAuthor() . "\n";
echo 'заглавие: ' . $oTitle->getTitle() . "\n\n";

$oAuthor->setAuthorLowerCase();

echo "After Author set to Lower Case: \n";
echo 'автор: ' . $oAuthor->getAuthor() . "\n";
echo 'заглавие: ' . $oTitle->getTitle() . "\n\n";

$oTitle->setTitleUpperCase();

echo "After Title set to Upper Case: \n";
echo 'автор: ' . $oAuthor->getAuthor() . "\n";
echo 'заглавие: ' . $oTitle->getTitle() . "\n\n";



echo "\n\n\n**************************************************\n\n\n";



/*class StockOffer
{
	private $stockShares = 0;
	private $stockSymbol = '';
	private $colleagueCode = 0;

	public function __construct($numOfShares, $stockSymb, $collgCode){
		$this->stockShares = $numOfShares;
		$this->stockSymbol = $stockSymb;
		$this->colleagueCode = $collgCode;
	}

	public function getStockShares(){
		return $this->stockShares;
	}
	public function getStockSymbol(){
		return $this->stockSymbol;
	}
	public function getCollCode(){
		return $this->colleagueCode;
	}
}


abstract class Colleague
{
	private $mediator;	// of class Mediator
	private $colleagueCode;

	public function __construct(Mediator $newMediator){
		// Всеки "колега" трябва да има Медиаторът като private пропърти
		$this->mediator = $newMediator;
	}

	public function setCollCode($collCode){
		$this->colleagueCode = $collCode;
	}
	public function saleOffer($stock, $shares){
		$mediator->saleOffer($stock, $shares, $this->colleagueCode);
	}
	public function buyOffer($stock, $shares){
		$mediator->buyOffer($stock, $shares, $this->colleagueCode);
	}
}


class GormanSlacks extends Colleague
{
	public function __construct(Mediator $newMediator){
		parent::__construct($newMediator);
		echo "\nGormanSlacks toku 6to se vklu4i na borsata\n";
	}
}
class JTPoorman extends Colleague
{
	public function __construct(Mediator $newMediator){
		parent::__construct($newMediator);
		echo "\JTPoorman toku 6to se vklu4i na borsata\n";
	}
}


interface Mediator {
	public function saleOffer($stock, $shares, $collCode);
	public function buyOffer($stock, $shares, $collCode);
	public function addColleague(Colleague $colleague);
}

class StockMediator implements Mediator
{
	private $colleagues = array();
	private $stockBuyOffers = array();
	private $stockSellOffers = array();

	private $colleagueCodes = 0;

	public function addColleague(Colleague $newColleague){
		$this->colleagues[] = $newColleague;
		$this->colleagueCodes++;
		$newColleague->setCollCode($this->colleagueCodes);
	}

	public function saleOffer($stock, $shares, $collCode){
		$stockSold = false;
		StockOffer offer

		for(){
			if(){

				$stockSold = true;
			}

			if($stockSold){
				break;
			}
		}

		if($stockSold === false){
			echo "\n \n";
			$newOffering = = new StockOffer($shares, $stock, $collCode);
		}
	}
	public function buyOffer($stock, $shares, $collCode){

	}
	public function addColleague(Colleague $colleague){

	}
}


$nysq = new StockMediator();

$gorman = new GormanSlacks($nyse);
$jtpoorman = new JTPoorman($nyse);

$gorman->saleOffer('MSFT', 100);
$gorman->saleOffer('GOOG', 50);

$jtpoorman->buyOffer('MSFT', 100);
$jtpoorman->buyOffer('NRG', 10);

$gorman->buyOffer('NRG', 10);

$nysq->getStockOfferings();*/
