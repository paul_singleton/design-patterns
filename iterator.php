<?php
/**
 * Този ДП е идеален когато имаш колекции различен тип (примерно масив, хеш...) от обекти от еднакъв тип, 
 * и трябва примерно да ги изциклиш.
 * Примерно имаш една колекция-масив от обекти от даден тип, примерно филми от 70-те,
 * и друга колекция от друг тип-хеш от филми от 80-те.
 * И трябва да можеш с един такъв итератор примерно да изцикляш и едните, и другите. Или примерно да отделиш 
 * първите 10 филма и автомобила.
 * Т.е. "to provide a uniform way of access different types of collections of objects".
 * Важното е, че обектите в отделните колекции трябва да са от един тип (клас), просто колекциите са различни, 
 * едната ще е масив, другата хещ...
 *
 * Правиш един интерфейс примерно IIterator където да има примерно един метод createIterator()
 * И всеки от класовете, където се създават тези колекции трябва да го имплементира.
 * И по този начин задължаваш всеки от тези класове, които създават свой тип колекция, да си итерират само колекцията.
 * "Имай си каквато искаш дейта струткура, просто трябва да можеш да си я изциклиш."
 */

declare(strict_types = 1);

class Book
{
	private string $author, $title;

	public function __construct(string $title = '', string $author = '')
	{
		$this->author = $title;
		$this->title  = $author;
	}

	private function getAuthor() : string
	{
		return $this->author;
	}

	private function getTitle() : string
	{
		return $this->title;
	}

	public function getAuthorAndTitle() : string
	{
		return $this->getTitle() . ' by ' . $this->getAuthor();
	}
}


/**
 * Това е класът, който ще представлява колекцията от книги, под формата на масив.
 * Методът "addBook(Book $oBook)" прави тази работа.
 * Отделно има разни гимнастики за брой книги ($bookCount) но те не са важни.
 */
class BookList
{
	private array $books = array();	// Тук всъщност ще пълним книгите (обектите на класа Book), това ни е колекцията. 
									// В този случай - масив.
	private int $bookCount = 0;

	public function getBookCount() : int
	{
		return $this->bookCount;
	}

	private function setBookCount(int $newCount) : void
	{
		$this->bookCount = $newCount;
	}

	// Връща книга по нейния индекс в масива с книги
	public function getBook(int $bookNumberToGet) : ?Book
	{
		if (is_numeric($bookNumberToGet) && ($bookNumberToGet <= $this->getBookCount())) {
			return $this->books[$bookNumberToGet];
		}
		return NULL;
	}

	public function addBook(Book $oBook) : int
	{
		$this->setBookCount($this->getBookCount() + 1);
		$this->books[$this->getBookCount()] = $oBook;
		return $this->getBookCount();
	}
}


interface IIterator
{
	public function hasNextBook() : bool;
	public function getNextBook() : ?Book;
}

/**
 * Това е класът-итератор, на който ще подадем горната колекция и той ще я изцикля.
 * Има разни методи, които примерно: увеличават някакъв вътрешен брояч, проверяват дали има следваща книга, 
 * вземат текущата...
 */
class BookListStraightIterator implements IIterator
{
	protected BookList $bookList;
	protected int $currentBook = 0;

	public function __construct(BookList $oBookList)
	{
		$this->bookList = $oBookList;
	}

	public function getNextBook() : ?Book
	{
		if($this->hasNextBook()){
			return $this->bookList->getBook(++$this->currentBook);
		}
		return NULL;
	}

	public function hasNextBook() : bool
	{
		if($this->bookList->getBookCount() > $this->currentBook){
			return true;
		}
		return false;
	}
}


class BookListReverseIterator implements IIterator
{
	public function __construct(BookList $oBookList)
	{
		$this->bookList = $oBookList;
		$this->currentBook = $this->bookList->getBookCount() + 1;
	}

	public function getNextBook() : ?Book
	{
		if ($this->hasNextBook()) {
			return $this->bookList->getBook(--$this->currentBook);
		}
		return NULL;
	}

	public function hasNextBook() : bool
	{
		if ($this->currentBook > 1) {
			return true;
		}
		return false;
	}
}


$books = new BookList();
$books->addBook(new Book('pyrva kniga: Vojna i Mir', 'Leonid Tolstoy'));
$books->addBook(new Book('vtora kniga: Pod Igoto', 'Ivan Vazov'));
$books->addBook(new Book('treta kniga: Mamino detentse', 'Luben Karavelov'));

echo "Testing the Straight Iterator \n\n";

$booksIterator = new BookListStraightIterator($books);

while ($booksIterator->hasNextBook()) {
	$book = $booksIterator->getNextBook();
	echo "getting next book with iterator : \n";
	echo $book->getAuthorAndTitle();
	echo "\n\n";
}


echo "\n\nTesting the Reverse Iterator \n\n";

$booksReverseIterator = new BookListReverseIterator($books);

while ($booksReverseIterator->hasNextBook()) {
	$book = $booksReverseIterator->getNextBook();
	echo "getting next book with REVERSE iterator : \n";
	echo $book->getAuthorAndTitle();
	echo "\n\n";
}



echo "\n\n\n**************************************************\n\n\n";



declare(strict_types = 1);

interface IIterate 
{
	public function hasNext() : bool;
	public function next() : ?string;
}

interface IContain 
{
	public function createIterator() : IIterate;
}

class BooksCollection implements IContain
{
	private array $a_titles = array();

	public function createIterator() : IIterate
	{
		return new BookIterator($this);
	}

	public function setTitle(string $string) : void
	{
		$this->a_titles[] = $string;
	}

	public function getTitles() : array
	{
		return $this->a_titles;
	}
}

class BookIterator implements IIterate
{
	private int $i_position = 0;
	private BooksCollection $booksCollection;

	public function __construct(BooksCollection $booksCollection)
	{
		$this->booksCollection = $booksCollection;
	}

	public function hasNext() : bool
	{
		if ($this->i_position < count($this->booksCollection->getTitles())) {
			return true;
		}
		return false;
	}

	public function next() : ?string
	{
		$m_titles = $this->booksCollection->getTitles();

		if ($this->hasNext()) {
			return $m_titles[$this->i_position++];
		}
		return null;
	}
}

$booksCollection = new BooksCollection();
$booksCollection->setTitle('Kniga edno');
$booksCollection->setTitle('Kniga dve');
$booksCollection->setTitle('Kniga tri');
$booksCollection->setTitle('Kniga 4etiri');

$iterator = $booksCollection->createIterator();
while ($iterator->hasNext()) {
	echo $iterator->next() . "\n";
}
