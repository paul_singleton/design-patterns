<?php
/**
 * Идеята е примерно ако имаш обект, който му се променят пропъртитата, и ти потрябва някаква минала стойност на едно 
 * или повече пропъртита.
 *
 * Състои се от 3 класа - Memento, Originator и CareTaker.
 *
 * Memento е този, в който искаме да запазваме предишни състояния, които после може да ни потрябват. 
 * Него го ползваме като шкафче, като за всяка стойност, която искаме да запазваме ще имаме едно пропърти (чекмедже).
 * Примерно искаме всеки час да имаме запазени дневните температура, влажност и атм. налягане.
 * На всеки час, трябва да създаваме нов обект Memento (ново шкафче) с три пропъртита (чекмедженца) за тези трите.
 * Това създаване на Memento обект ще става в класа Originator, с след това този нов Memento обект се подава
 * на метод на обект от CareTaker за да го добави в масива с другите Memento обекти.
 *
 * Originator е този, който set-ва и get-ва стойности, които искаме да имаме запазени ако ни потрябват после.
 * На него му ги подаваш и той прави нов обект Memento като ги запазва като негови пропъртита.
 *
 * CareTaker създава и поддържа масив (като private пропърти) с всички Memento обекти. Има един метод add() за добавяне на ново Memento и един метод get(),
 * на който подаваш индекс, за връщане на търсеното Memento.
 *
 * Memento класът примено ще е някакъв съвсем прост клас с едно (или повече) private пропърти, на конструктура му подаваш
 * стойност да го сетне, отделно имаш и един get-ър да го връща (виж втория пример).
 * Може и повече пропъртита да има, и за всяко по един get-ът и set-ър.
 *
 * Естествено всичко това може и да се направи много по лесно, примерно с един масив и там да пълним стари 
 * стойности, които може да ни трябват за по-късно.
 */

declare(strict_types = 1);

class Memento
{
	private string $state;

	public function __construct(string $state)
	{
		$this->state = $state;
	}

	public function getState() : string
	{
		return $this->state;
	}

	public function setState(string $state)
	{
		$this->state = $state;
	}
}


class Originator
{
	private string $state;

	public function setState(string $state = '') : void
	{
		$this->state = $state;
	}

	public function getState() : string
	{
		return $this->state;
	}

	public function saveStateToMemento() : Memento
	{
		return new Memento($this->state);
	}

	public function getStateFromMemento(Memento $memento) : void
	{
		$this->state = $memento->getState();
	}
}


class CareTaker
{
	private array $aMemento = array();

	public function addMemento(Memento $state) : void
	{
		$this->aMemento[] = $state;
	}

	public function getMemento(int $index = 0) : Memento
	{
		return $this->aMemento[$index];
	}
}


$originator = new Originator();
$careTaker = new CareTaker();

$originator->setState('Some example state #0');
$careTaker->addMemento($originator->saveStateToMemento());

$originator->setState('Some example state #1');
$careTaker->addMemento($originator->saveStateToMemento());

var_dump($careTaker->getMemento(1));

$originator->getStateFromMemento($careTaker->getMemento(0));
echo $originator->getState();





echo "\n\n\n**************************************************\n\n\n";





declare(strict_types = 1);


/**
 * Това ще ни е класът, на когото ще пазим отделните "моментни снимки" с оглед на това да можем да възстановяваме
 * когато ни трябва. Тоест, заради него ще прилагаме самият Memento Design Pattern.
 * Съществените методи са "saveToMemento()" и "restoreFromMemento()"
 */
class Originator
{
	private string $state;

	// The class could also contain additional data that is not part of the Memento Design Pattern

	public function set(string $state) : void
	{
		$this->state = $state;

		printf("%s\n\n", 'Originator: Setting state to ' . $state);
	}

	public function saveToMemento() : Memento
	{
		printf("%s\n\n", 'Originator: Saving to Memento.');

		return new Memento($this->state);
	}

	public function restoreFromMemento(Memento $memento) : void
	{
		$this->state = $memento->getSnapshot();

		printf("%s\n\n", 'Originator: State after restoring from Memento: ' . $this->state);
	}
}


/**
 * Toзи клас ще играе ролята на една прост data container, всеки обект от който ще е просто една "моментна снимка",
 * по този начин информацията ("моментната снимка") ще е "скрита" от околният свят (data hidding).
 */
class Memento
{
	private string $snapshot;

	public function __construct(string $snapshotToSave)
	{
		$this->snapshot = $snapshotToSave;
	}

	public function getSnapshot() : string
	{
		return $this->snapshot;
	}
}


/**
 * Toва ще е така да се каже "диригентът" на това да създаваме момнтни снимки, и да можем да ги възстановяваме.
 */
class Caretaker
{
	private array $savedStates = array();		// This will be the array with the mementos

	private Originator $originator;

	public function __construct(Originator $originator)
	{
		$this->originator = $originator;
	}

	public function takeCare() : void
	{
		$this->originator->set("State 1");

		$this->originator->set("State 2");

		// Push a memento, или още "Запази се себе си в едно Мементо и го дай да го пушнем в $savedStates"
		array_push($this->savedStates, $this->originator->saveToMemento());

		$this->originator->set("State 3");

		$this->originator->set("State 4");

		// Push a memento, или още "Направи си селфи и ми го дай да го пушна в $savedStates"
		array_push($this->savedStates, $this->originator->saveToMemento());

		$this->originator->set("State 5");

		$this->originator->restoreFromMemento($this->savedStates[1]);
	}
}


(new Caretaker(new Originator))->takeCare();
