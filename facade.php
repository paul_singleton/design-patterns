<?php
/**
 * Фасада е един вид "франкенщайн" от парчетии от разни, напълно независими един от друг класове.
 *
 * "Provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use."
 * (Gang Of Four)
 *
 * Този ДП е подходящ когато имаме сложна система от класове, примерно някакво library като phpThumb... изобщо нещо, на което трябва да викаме повече методи,
 * да създаваме повече обекти и т.н...
 * Тогава можем да си напишем един Facade клас, където да отделим всичко това в един метод, и само да го викаме него.
 * Затова и така се казва - Фасада, зад която можеш да си напишеш хиляди неща.
 *
 * В този пример имаш един прост клас Book, имаш и 2 класа, които имат задача да обърнат кейса на автор/заглавието на книгите.
 * Ако им подадеш "Ивлин Уо - Завръщане в Брайдсхед", да ти върнат "иВЛИН уО - зАВРЪЩАНЕ В бРАЙДСХЕД"
 * Нарочно е направено с 2 класа, като естествено може и много по-лесно, примерно само с един.
 * Класът CaseReverseFacade е самата т.н. Фасада, там извикваш (в метода reverseStringCase()) всички методи на горните два (или колкото са) класа,
 * които реално вършат работата.
 * Ми това е, после само викаш само CaseReverseFacade::reverseStringCase()
 *
 * И да не забравяме, че във Фасадата можем да викаме методи на НАПЪЛНО НЕЗАВИСИМИ ЕДИН ОТ ДРУГ КЛАСОВЕ, по този начин
 * "сглобявайки" си своя логика от различни "парчета".
 * От този клас вземи този и този метод, от онзи клас вземи онзи и онзи метод...
 */

class Book
{
	private string $author, $title;

	public function __construct(string $title_in = '', string $author_in = ''){
		$this->author = $author_in;
		$this->title  = $title_in;
	}

	private function getAuthor() : string
	{
		return $this->author;
	}

	private function getTitle() : string
	{
		return $this->title;
	}

	public function getAuthorAndTitle() : string
	{
		return $this->getTitle() . ' by ' . $this->getAuthor();
	}
}


class CaseReverseFacade
{
	public static function reverseStringCase(string $str = '') : string
	{
		// разбиваш стринг на масив от символи
		$aStr = ArrayStringFunctions::stringToArray($str);

		// взема горния масив и сменя кейса на всеки символ, ако е голям - в малък, и обратното
		$aStrRev = ArrayCaseReverse::reverseCase($aStr);

		// от горния масив прави отново стринг
		$sStrRev = ArrayStringFunctions::arrayToString($aStrRev);

		return $sStrRev;
	}
}


class ArrayCaseReverse
{
	const A_UPPER = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
	const A_LOWER = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');

	public static function reverseCase($arrayIn) : array
	{
		$aOut = array();

		for($x = 0; $x < count($arrayIn); $x++){
			if(in_array($arrayIn[$x], self::A_UPPER)){
				$key = array_search($arrayIn[$x], self::A_UPPER);
				$aOut[$x] = self::A_LOWER[$key];
			} elseif(in_array($arrayIn[$x], self::A_LOWER)){
				$key = array_search($arrayIn[$x], self::A_LOWER);
				$aOut[$x] = self::A_UPPER[$key];
			} else {
				$aOut[$x] = $arrayIn[$x];
			}
		}

		return $aOut;
	}
}


class ArrayStringFunctions
{
	// Прави стринг от масив
	public static function arrayToString(array $arrayIn) : string
	{
		return implode('', $arrayIn);
	}

	// Разбива стринг на масив от символи
	public static function stringToArray(string $str = '') : array
	{
		return str_split($str);
	}
}


$book = new Book('Design Patterns', 'Gamma, Helm, Johnson, and Vlissides');
echo 'Original book author/title: ' . $book->getAuthorAndTitle();

echo "\n\n";

$bookTitleReversed = CaseReverseFacade::reverseStringCase($book->getAuthorAndTitle());
echo 'Reversed book author/title: ' . $bookTitleReversed;
