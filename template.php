<?php
/**
 * Имаш много близки като структура класове.
 * Правиш един абстрактен клас-родител и всичко което е общо и еднакво за наследниците го отделяш като final методи в родителя.
 * Декларираш и съвкупност от абстрактни методи, за да задължиш всеки от наследниците да ги дефинира (дори и като празни).
 * В родителя правиш един метод, наречен "template method" (напр. "makeSandwich()"), който с някакъв алгоритъм да използва както общите (final) методи,
 * така и тези, които трябва да са дефинирани в наследниците.
 * Този "template method" не трябва да се пренаписва според логиката, а по-скоро логиката трябва да се задава примерно с флагове като "customerWants..."
 * В долният пример, това кой/кои методи да се изпълнят в "makeSandwich()", се определя от методи-флагове ("customerWants..."),
 * които се дефинират в класовете-наследници.
 * С други думи, класовете наследници казват на "template method"-a кои точно методи да изпълни, т.е. наследниците определят логиката в "template method".
 * "Аз съм еди-какъв си сандвич и тярбва да съдържам това и това, действай."
 */

abstract class Hoagie
{
	// Това са основните части на един сандвич.
	// Може да са дефинирани с по едно "return true;" за да приемем, че по дефолт всеки сандвич ще има месо, сирене, зеленчуци и подправки.
	// По-добре обаче е да са абстрактни за да задължим класовете-наследници изрично да ги дефинират и да зададат дали искат всяка една от съставките.
	abstract protected function customerWantsMeat();
	abstract protected function customerWantsCheese();
	abstract protected function customerWantsVegetables();
	abstract protected function customerWantsCondiments();


	// Всеки клас-наследник е длъжен да дефинира свои дефиниции за тези методи, дори и да са празни.
	// "addMeat()" примерно ще е празен за вегетарианските сандвичи...
	abstract protected function addMeat();
	abstract protected function addCheese();
	abstract protected function addVegetables();
	abstract protected function addCondiments();


	// Това е самият метод, който прави сандвича, тук описваш стандартната процедура за направата на всички видове сандвичи.
	// "final" защото не искаме наследниците да го override-ват.
	final public function makeSandwich(){
		$this->cutBun();	// Няма сандвич без питка, която да срежем на две

		if($this->customerWantsMeat()){
			$this->addMeat();
		}
		if($this->customerWantsCheese()){
			$this->addCheese();
		}
		if($this->customerWantsVegetables()){
			$this->addVegetables();
		}
		if($this->customerWantsCondiments()){
			$this->addCondiments();
		}

		$this->wrapTheHoagie();		// Накря всеки сандвич трябва да се опакова
	}

	final protected function cutBun(){
		echo "Pitkata e srqzana \n";
	}

	final protected function wrapTheHoagie(){
		echo "Sandvichyt e opakovan, dobyr apetit \n";
	}
}


class ItalianHoagie extends Hoagie
{
	// В италианските сандвичи има всичко ;-)))
	protected function customerWantsMeat(){ return true; }
	protected function customerWantsCheese(){ return true; }
	protected function customerWantsVegetables(){ return true; }
	protected function customerWantsCondiments(){ return true; }

	protected function addMeat(){
		echo "Mesoto e dobaveno \n";
	}
	protected function addCheese(){
		echo "Sireneto e dobaveno \n";
	}
	protected function addVegetables(){
		echo "Zelenchutsite sa dobaveni \n";
	}
	protected function addCondiments(){
		echo "Podpravkite sa dobaveni \n";
	}
}


class VeggieHoagie extends Hoagie
{
	// Във вегетарианските сандвичи има само зеленчуци и подправки
	protected function customerWantsMeat(){ return false; }
	protected function customerWantsCheese(){ return false; }
	protected function customerWantsVegetables(){ return true; }
	protected function customerWantsCondiments(){ return true; }

	// addMeat() и addCheese() може и да са дефинирани така или иначе няма да бъдат изпълнени, защото за този клас "customerWantsMeat()" е false
	protected function addMeat(){ return null; }
	protected function addCheese(){ return null; }
	protected function addVegetables(){
		echo "Zelenchutsite sa dobaveni \n";
	}
	protected function addCondiments(){
		echo "Podpravkite sa dobaveni \n";
	}
}


$ih = new ItalianHoagie();
$ih->makeSandwich();

$vh = new VeggieHoagie();
$vh->makeSandwich();



echo "\n\n\n**************************************************\n\n\n";



abstract class TemplateAbstract
{
	// the template method
	// sets up a general algorithm for the whole class
	final public function showBookTitleInfo($book_in){
		$title = $book_in->getTitle();
		$author = $book_in->getAuthor();
		$processedTitle = $this->processTitle($title);
		$processedAuthor = $this->processAuthor($author);
		if($processedAuthor == null){
			$processed_info = $processedTitle;
		} else {
			$processed_info = $processedTitle . ' by ' . $processedAuthor;
		}
		return $processed_info;
	}

	// the primitive operation
	// this function MUST be overridded
	abstract public function processTitle($title);

	// the hook operation
	// this function may be overridden,
	// but does nothing if it is not
	public function processAuthor($author){
		return null;
	}
}

class TemplateExclaim extends TemplateAbstract
{
	public function processTitle($title){
		return str_replace(' ', '!!!', $title);
	}

	public function processAuthor($author){
		return str_replace(' ', '!!!', $author);
	}
}

class TemplateStars extends TemplateAbstract
{
	public function processTitle($title){
		return str_replace(' ', '*', $title);
	}
}


class Book
{
	private $author, $title;

	public function __construct($title_in = '', $author_in = ''){
		$this->author = $author_in;
		$this->title = $title_in;
	}

	public function getAuthor(){
		return $this->author;
	}
	public function getTitle(){
		return $this->title;
	}
	public function getAuthorAndTitle(){
		return $this->getTitle() . ' by ' . $this->getAuthor();
	}
}


$book = new Book('PHP for Cats', 'Larry Truett');

$exclaimTemplate = new TemplateExclaim();
$starsTemplate = new TemplateStars();

echo 'test 1 - show exclaim template' . "\n";
echo $exclaimTemplate->showBookTitleInfo($book);

echo 'test 2 - show stars template' . "\n";
echo $starsTemplate->showBookTitleInfo($book);
