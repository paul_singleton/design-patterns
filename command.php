<?php
/**
 * Има добър пример с едно дистанционно, където трябва веднъж при натискането на копчетата всяко да прави това и това,
 * а друг път при натискане на същите копчета - друго и друго.
 * Всяко копче ще е обект и ще има 2 метода on() и off() (което значи, че ще имплементират един абсклас или интерфейс),
 * но самото действие което става при натискане на копчето ще е изнесено другаде.
 */


declare(strict_types = 1);

/**
 * Това е класът, който ще бъде комaндван и ще изпълнява бизнес логиката.
 * Toй си е "отворил" три публични метода, през които ще бъдат викани за да получава команди.
 */
class BookCommandee
{
	private string $author, $title;

	public function __construct(string $title = '', string $author = '')
	{
		$this->setAuthor($author);
		$this->setTitle($title);
	}


	// Малко гетъри и сетъри...
	private function getAuthor() : string
	{
		return $this->author;
	}
	private function setAuthor(string $author = '') : void
	{
		$this->author = $author;
	}

	private function getTitle() : string
	{
		return $this->title;
	}
	private function setTitle(string $title = '') : void
	{
		$this->title = $title;
	}


	public function getAuthorAndTitle() : string
	{
		return $this->getTitle() . ' by ' . $this->getAuthor();
	}


	// Тези трите са самите "команди", които нашият командван клас ще изпълнява.
	// Tоест, нещата, които ще се случват, когато го изкомандваме.
	// Ще ги викаме от някакъв друг клас-команда (напр. BookStarsOnCommand или BookStarsOffCommand),
	// които пък ще extend-ват един абстрактен клас "BookCommand".
	public function doStarsOn() : void
	{
		$this->setAuthor(str_replace(' ', '*', $this->getAuthor()));
		$this->setTitle(str_replace(' ', '*', $this->getTitle()));
	}

	public function undoStarsOn() : void
	{
		$this->setAuthor(str_replace('*', ' ', $this->getAuthor()));
		$this->setTitle(str_replace('*', ' ', $this->getTitle()));
	}

	public function setUppercase() : void
	{
		$this->setAuthor(strtoupper($this->getAuthor()));
		$this->setTitle(strtoupper($this->getTitle()));
	}
}


/* Това ще са командите */
abstract class BookCommand
{
	protected BookCommandee $bookCommandee;

	public function __construct(BookCommandee $oBookCommandee)
	{
		$this->bookCommandee = $oBookCommandee;
	}

	abstract public function execute() : void;
}

class BookStarsOnCommand extends BookCommand
{
	public function execute() : void
	{
		$this->bookCommandee->doStarsOn();
	}
}

class BookStarsOffCommand extends BookCommand
{
	public function execute() : void
	{
		$this->bookCommandee->undoStarsOn();
	}
}

class BookStarsUppercase extends BookCommand
{
	public function execute() : void
	{
		$this->bookCommandee->setUppercase();
	}
}


$book = new BookCommandee('Ultimate survival', 'Bear Gryls');

echo "Original book after its creation: \t" . $book->getAuthorAndTitle() . "\n\n";

// Eто ти обект, изпълни му "doStarsOn()" метода, минавайки през "execute()"
$starsOn = (new BookStarsOnCommand($book))->execute();
echo "Book after stars on: \t\t\t" . $book->getAuthorAndTitle() . "\n\n";

// Eто ти го пак, изпълни му "undoStarsOn()" метода, минавайки през "execute()"
$starsOff = (new BookStarsOffCommand($book))->execute();
echo "Book after stars off: \t\t\t" . $book->getAuthorAndTitle() . "\n\n";

// Eто ти го пак, изпълни му "setUppercase()" метода, минавайки през "execute()"
$upper = (new BookStarsUppercase($book))->execute();
echo "Book after uppercase: \t\t\t" . $book->getAuthorAndTitle() . "\n\n";

// Всеки един от тези три реда по-горе, е все едно едно натискане на бутонче:
// "Сложи звезди", "Undo", "С главни"
