<?php
/**
 * Имаме група обекти, които работят в комбина, т.е. всеки прави своята си работа, но си предават действието,
 * което трябва да се извърши един на друг,
 * докато не се намери кой да го изпълни. т.е. работата, която трябва да се свърши е една, но не се знае кой точно обект да я свърши.
 * Като в Държавната администрация - всеки те прехвърля на другия. Или за дадено нещо трябва да минеш през 5 гишета.
 * Или като една стара игра, където пускаш отгоре едно топче и то минава през различни възможни пътища, през различни разклонения,
 * но предварително си задал според някаква конкретна логика на къде да поеме.
 * 
 * Затова си правим набор от класове и ръчно задаваме кой след кого е във въпросната "верига на отговорности".
 * И подаваме задачата на първия. В него първо проверяваме дали той може да я свърши и ако не - предаваме задачата на следващия.
 *
 * Първо, имаш един интерфейс ChainInterface където имаш декларирани 2 метода, setNextChainLink() и doTheJob(),
 * личи си от имента им кой какво прави.
 * В setNextChainLink() ще се задава следващият елемент от веригата, а в doTheJob() ще проверяваш дали е за него работата и ако да - свърши я,
 * ако не - предай нататък.
 * Пак повтатям - задаването на "веригата от отговорности" става ръчно. Примерно, правим си 5 класа, които трябва да имплементират
 * интерфейса ChainInterface, и всеки ще има едно private пропърти $nextChainLink където в конструктора, като създаваме този обект,
 * ще му подаваме обект, създаден преди.
 *
 * Този design pattern е много удобен за избягване от заплетена логика, вложени IF-ове и т.н...
 * просто трябва логиката да се организира "поточно".
 */

declare(strict_types = 1);

interface ChainInterface
{
	public function setNextChainLink(ChainInterface $oChainLink);
	public function doTheJob($jobToBeDone);
}

class Responsible1 implements ChainInterface
{
	private ChainInterface $nextChainLink;

	public function setNextChainLink(ChainInterface $oChainLink = NULL){
		if (empty($oChainLink)) {
			// do smth...
		} else {
			$this->nextChainLink = $oChainLink;
		}
	}

	public function doTheJob($jobToBeDone){
		if ($jobToBeDone['job'] == 'add') {
			return $jobToBeDone['tova'] + $jobToBeDone['onova'];
		}

		if (empty($this->nextChainLink)) {
			return 'Stignahme do kraq i ne namerih ChainInterface, kojto da obraboti tazi zada4a.';
		}

		return $this->nextChainLink->doTheJob($jobToBeDone);
	}
}

class Responsible2 implements ChainInterface
{
	private ChainInterface $nextChainLink;

	public function setNextChainLink(ChainInterface $oChainLink = NULL){
		if (empty($oChainLink)) {
			// do smth...
		} else {
			$this->nextChainLink = $oChainLink;
		}
	}

	public function doTheJob($jobToBeDone){
		if ($jobToBeDone['job'] == 'sub') {
			return $jobToBeDone['tova'] - $jobToBeDone['onova'];
		}

		if (empty($this->nextChainLink)) {
			return 'Stignahme do kraq i ne namerih ChainInterface, kojto da obraboti tazi zada4a.';
		}

		return $this->nextChainLink->doTheJob($jobToBeDone);
	}
}

class Responsible3 implements ChainInterface
{
	private ChainInterface $nextChainLink;

	public function setNextChainLink(ChainInterface $oChainLink = NULL){
		if (empty($oChainLink)) {
			// do smth...
		} else {
			$this->nextChainLink = $oChainLink;
		}
	}

	public function doTheJob($jobToBeDone){
		if ($jobToBeDone['job'] == 'mul') {
			return $jobToBeDone['tova'] * $jobToBeDone['onova'];
		}

		if (empty($this->nextChainLink)) {
			return 'Stignahme do kraq i ne namerih ChainInterface, kojto da obraboti tazi zada4a.';
		}

		return $this->nextChainLink->doTheJob($jobToBeDone);
	}
}

$resp1 = new Responsible1();
$resp2 = new Responsible2();
$resp3 = new Responsible3();

$resp1->setNextChainLink($resp2);
$resp2->setNextChainLink($resp3);

$res = $resp1->doTheJob(array('tova' => 4, 'onova' => 3, 'job' => 'add'));
var_dump($res);





echo "\n\n\n**************************************************\n\n\n";





abstract class AbstractBookTopic
{
	abstract public function getTopic() : string;
	abstract public function getTitle() : string;
	abstract public function setTitle($title_in) : void;
}


class BookTopic extends AbstractBookTopic
{
	private string $topic;
	private ?string $title;

	public function __construct(string $topic_in)
	{
		$this->topic = $topic_in;
		$this->title = NULL;
	}

	public function getTopic() : string
	{
		return $this->topic;
	}

	// This is the end of the chain - returns title or says there is none
	public function getTitle() : string
	{
		if(NULL != $this->title){
			return $this->title;
		}
		return 'Тhere is no title available';
	}

	public function setTitle($title_in) : void
	{
		$this->title = $title_in;
	}
}


class BookSubTopic extends AbstractBookTopic
{
	private string $topic;
	private BookTopic $parentTopic;
	private ?string $title;

	public function __construct(string $topic_in, BookTopic $parentTopic_in){
		$this->topic = $topic_in;
		$this->parentTopic = $parentTopic_in;
		$this->title = NULL;
	}

	public function getTopic() : string
	{
		return $this->topic;
	}

	public function getParentTopic() : BookTopic
	{
		return $this->parentTopic;
	}

	public function getTitle() : string
	{
		if(NULL != $this->title){
			return $this->title;
		}
		return $this->parentTopic->getTitle();
	}

	public function setTitle($title_in) : void
	{
		$this->title = $title_in;
	}
}


class BookSubSubTopic extends AbstractBookTopic
{
	private string $topic;
	private BookSubTopic $parentTopic;
	private ?string $title;

	public function __construct(string $topic_in, BookSubTopic $parentTopic_in){
		$this->topic = $topic_in;
		$this->parentTopic = $parentTopic_in;
		$this->title = NULL;
	}

	public function getTopic() : string
	{
		return $this->topic;
	}

	public function getParentTopic() : BookSubTopic
	{
		return $this->parentTopic;
	}

	public function getTitle() : string
	{
		if(NULL != $this->title){
			return $this->title;
		}
		return $this->parentTopic->getTitle();
	}

	public function setTitle($title_in) : void
	{
		$this->title = $title_in;
	}
}


$bookTopic = new BookTopic('PHP 5');
echo "bookTopic before title is set: \n";
echo 'topic: ' . $bookTopic->getTopic() . "\n";
echo 'title: ' . $bookTopic->getTitle() . "\n";
echo "\n\n";

$bookTopic->setTitle('PHP 5 Recipes by Babin, Good, Kroman, and Stephens');
echo "bookTopic after title is set: \n";
echo 'topic: ' . $bookTopic->getTopic() . "\n";
echo 'title: ' . $bookTopic->getTitle() . "\n";
echo "\n\n";

$bookSubTopic = new BookSubTopic('PHP 5 Patterns', $bookTopic);
echo "bookSubTopic before title is set: \n";
echo 'topic: ' . $bookSubTopic->getTopic() . "\n";
echo 'title: ' . $bookSubTopic->getTitle() . "\n";
echo "\n\n";

$bookSubTopic->setTitle('PHP 5 Objects Patterns and Practice by Zandstra');
echo "bookSubTopic after title is set: \n";
echo 'topic: ' . $bookSubTopic->getTopic() . "\n";
echo 'title: ' . $bookSubTopic->getTitle() . "\n";
echo "\n\n";

$bookSubSubTopic = new BookSubSubTopic('PHP 5 Patterns for Cats', $bookSubTopic);
echo "bookSubSubTopic with no title set: \n";
echo 'topic: ' . $bookSubSubTopic->getTopic() . "\n";
echo 'title: ' . $bookSubSubTopic->getTitle() . "\n";
echo "\n\n";

$bookSubTopic->setTitle(NULL);
echo "bookSubSubTopic with no title for set for bookSubTopic either: \n";
echo 'topic: ' . $bookSubSubTopic->getTopic() . "\n";
echo 'title: ' . $bookSubSubTopic->getTitle() . "\n";
echo "\n\n";
