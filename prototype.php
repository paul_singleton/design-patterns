<?php
/**
 * Не създавай нов обект, клонирай вече създаден.
 * Подходящ когато ни трябват много близки обекти, създаваме си един клас, който да е прототип и после само го клонираме.
 * Положителното е, че клонирането понякога е около 3 пъти по-бързо от създаването.
 *
 */

declare(strict_types = 1);

abstract class BookPrototype
{
	protected string $title;
	protected string $topic;

	abstract public function __clone();

	public function getTitle() : string
	{
		return $this->title;
	}

	public function setTitle(string $titleIn = '') : void
	{
		$this->title = $titleIn;
	}

	public function getTopic() : string
	{
		return $this->topic;
	}
}


class PHPBookPrototype extends BookPrototype
{
	public function __construct()
	{
		$this->topic = 'PHP';
	}

	public function __clone(){}
}


class SQLBookPrototype extends BookPrototype
{
	public function __construct()
	{
		$this->topic = 'SQL';
	}

	public function __clone(){}
}


$phpProto = new PHPBookPrototype();
$sqlProto = new SQLBookPrototype();

$book1 = clone $sqlProto;
$book1->setTitle('The magic of SQL');
echo 'Book 1 topic: ' . $book1->getTopic() . "\n";
echo 'Book 1 title: ' . $book1->getTitle() . "\n";

echo "\n";

$book2 = clone $phpProto;
$book2->setTitle('PHP for dummies');
echo 'Book 2 topic: ' . $book2->getTopic() . "\n";
echo 'Book 2 title: ' . $book2->getTitle() . "\n";

echo "\n";

$book3 = clone $sqlProto;
$book3->setTitle('Stored procedures in SQL');
echo 'Book 3 topic: ' . $book3->getTopic() . "\n";
echo 'Book 3 title: ' . $book3->getTitle() . "\n";





echo "\n\n\n**************************************************\n\n\n";





class ComputerType
{
	public const NONE = 0;
	public const PDA = 1;
	public const NOTEBOOK = 2;
	public const DESKTOP = 3;
	public const CRAY = 4;
	public const MIN = 0;
	public const MAX = 4;
}

class Computer
{
	static private array $pool = array();
	static private array $count = array();
	private int $type = ComputerType::NONE;

	protected function __construct(int $t)
	{
		if (($t < ComputerType::MIN) || ($t > ComputerType::MAX)) {
			die("Invalid type $t");
		}
		$this->type = $t;
	}

	static public function getPDA()
	{
		if (empty(self::$pool[ComputerType::PDA])) {
			self::$pool[ComputerType::PDA] = new Computer(ComputerType::PDA);
			self::$count[ComputerType::PDA] = 0;
		}
		return clone self::$pool[ComputerType::PDA];
	}

	public function __destruct()
	{
		self::$count[$this->type]--;
	}

	protected function __clone()
	{
		self::$count[$this->type]++;
	}
}

var_dump($a = Computer::getPDA());
var_dump($b = Computer::getPDA());
echo 'Equal: '; var_dump($a === $b);
