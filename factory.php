<?php
/**
 * "Define an interface for creating an object, but let subclasses decide which class to instantiate.
 * The Factory method lets a class defer instantiation it uses to subclasses." (Gang Of Four)
 *
 * When you need to create an object, lets encapsulate this in a method so that we have a control over this object creation and imply some logic if necessary.
 * В този смисъл, може примерно и да не знаем предварително кой клас да инстанцираме, и затова да имаме някаква логика, която да реши.
 * Защото ако знаехме предварително, тогава защо просто не си го инджектнем, нямаме нужда от Фактори Патерн.
 *
 * Какво е Фактори? Един клас (Simple Factory) или повече класове, наследяващо абсклас/интерфейс,
 * в който има ЕДИН метод-фактори, в който се създава и връща обект, като около създаването може да има различна логика,
 * в смисъл този фактори метод да не е само едно return new $className, а да има например валидации... Сега си мисля, че в този смисъл
 * Сингълтън много прилича на Фактори, защото преди да съзаде себе си, проверява дали вече се е създал. Но пък Сингълтъ създава себе си,
 * не му даваш име на клас както Фактори.
 *
 * Може да има много подобни клас-факторита, където в съответните си фактори-методи да се създават обекти по различна логика.
 * Следователно трябва да има абсклас или интерфейс, който тези отделни факторита да имплементират, за да имат обща структура.
 * Като се каже Фактори, не се има предвид само един клас-фактори (Simple Factory DP), може да са повече, но от един абсклас/интерфейс.
 * Ето ти го "интерфейсът за създаване на обект", ето ти и това, че "подкласовете на този интерфейс решават кой обект да съзададат",
 * използвайки своята логика (виж определението по-горе).
 * Демек, създаваш група факторита, имплементиращи даден(и) интерфейс(и), всяка от които факторита има своя логика, която решава кой обект да създаде.
 * Следователно, тези отделни логики дават смисъл от отделните факторита, защото ако във всяко от тях има само по един return new ...
 * какъв е смисълът да са повече от една факторитата. Тогава и Simple Factory ще свърши работа.
 *
 * Служи за случаите, в които искаме не само да създадем нов обект с new,
 * но и да добавим някаква логика, в която се вика това new. Демек, да направим wrapper около създаването на обект.
 * Factory DP може да се разглежда като нещо като wrapper around the new, around the instatiation itself.
 * When you are about to instantiate, lets encapsulate that instantiation.
 * Затова се нарича Фабрика - фабрика за обекти.
 *
 * Фактори се използва когато не знаеш предварително кой клас да инстанцираш, а се определя рънтайм, което значи, че
 * трябва някаква логика, която да определи кой клас да се инстанцира. Ако знаеш преварително кой клас да инстанцираш, няма да три трябва фактори,
 * просто си го инстанцирай.
 *
 * Има три вида Фактори ДП-и: Simple Factory DP, Factory Method DP и Abstract Factory DP
 *
 * Simple Factory е най-простото фактори, което можеш да си представиш - един клас с метод, на който подаваш името на класа, който да произведеш.
 * На ти името на класа, дай ми обекта, толкоз. In simple factory pattern, we have a factory class which has a method that returns different
 * types of object based on given input. Няма както по-горе - интерфейс който да имплементира, няма и други факторита, които също да го имплементират.
 * Общо взето - фактори-единак.
 */

abstract class ShapeFaabrika
{
	static public function getInstance($cName){
		return new $cName();
	}
	abstract public function draw();
}

class Circle extends ShapeFaabrika
{
	protected function __construct(){
		echo "Circle created \n";
	}
	public function draw(){}
}

class Rectangle extends ShapeFaabrika
{
	protected function __construct(){
		echo "Rectangle created \n";
	}
	public function draw(){}
}

class Triangle extends ShapeFaabrika
{
	protected function __construct(){
		echo "Triangle created \n";
	}
	public function draw(){}
}

$a = ShapeFaabrika::getInstance('Circle');
$b = ShapeFaabrika::getInstance('Rectangle');
var_dump($a, $b);
$c = new Circle();		// Fatal error: Call to protected Circle::__construct() from invalid context
