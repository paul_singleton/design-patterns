<?php

declare(strict_types = 1);

/**
 * Задаваме какво задължително всеки робот трябва да има...
 */
interface RobotCommonInterface
{
	public function setRobotHead(string $head) : void;
	public function setRobotTorso(string $torso) : void;
	public function setRobotArms(string $arms) : void;
	public function setRobotLegs(string $legs) : void;
}

/**
 * ...и го дефинираме.
 */
class Robot1 implements RobotCommonInterface
{
	private string $robotHead, $robotTorso, $robotArms, $robotLegs;

	public function setRobotHead(string $head) : void
	{
		$this->robotHead = $head;
	}

	public function setRobotTorso(string $torso) : void
	{
		$this->robotTorso = $torso;
	}

	public function setRobotArms(string $arms) : void
	{
		$this->robotArms = $arms;
	}

	public function setRobotLegs(string $legs) : void
	{
		$this->robotLegs = $legs;
	}
}


/**
 * Сега нека си зададем какво всеки "Билдър", който ще произвежда роботи, 
 * трябва да може да прави.
 *
 * Понеже идеята е да имаме много модели роботи, нека зададем общ интерфейс.
 */
interface RobotBuilderInterface
{
	public function buildRobotHead() : self;
	public function buildRobotTorso() : self;
	public function buildRobotArms() : self;
	public function buildRobotLegs() : self;

	public function getRobot() : RobotCommonInterface;
}

/**
 * И да създадем един "Билдър", който ще задава как точно се прави
 * конкеретен модел роботи.
 */
class Т1000RobotBuilder implements RobotBuilderInterface
{
	private RobotCommonInterface $robot;

	public function __construct(RobotCommonInterface $robot)
	{
		$this->robot = $robot;
		// Всъщност, може и тук да си го направим с $this->robot = new Robot1(),
		// което напълно да "скрие" целият процес на създаване на роботи.
		// Но аз предпочитам да инджектвам отвън
	}

	public function buildRobotHead() : self
	{
		// щом иаме създаден/инджектнат "гол" робот, 
		// започваме да му дефинираме отделните части...
		$this->robot->setRobotHead('Някаква глава от течен метал бла бла...');
		return $this;
	}

	public function buildRobotTorso() : self
	{
		$this->robot->setRobotTorso('Някакво тяло от течен метал бла бла...');
		return $this;
	}

	public function buildRobotArms() : self
	{
		$this->robot->setRobotArms('Някакви ръце от течен метал бла бла...');
		return $this;
	}

	public function buildRobotLegs() : self
	{
		$this->robot->setRobotLegs('Някакви крака от течен метал бла бла...');
		return $this;
	}

	public function getRobot() : RobotCommonInterface
	{
		return $this->robot;
	}
}


/**
 * Вижда се, че "Билдъра" Т1000RobotBuilder прави отделните части, 
 * но не и да сглобява целият робот, който ни трябва.
 */

/**
 * Единият начин да сглобим целият робот е да използваме директно getRobot() на Т1000RobotBuilder,
 * и там да сглобим робота, като "чейнем на влакче" отделните build... методи.
 * И разбира се да върнем готовия робот.
 * Нещо такова:
 */
$t1000 = (new Т1000RobotBuilder(new Robot1))
	->buildRobotHead()	// може и да подаваме различните свойства на Т-1000 с аргументи
	->buildRobotTorso() // вместо да са хардкоднатиу в класа...
	//->buildRobotArms()
	->buildRobotLegs();
var_dump($t1000->getRobot());



/**
 * Другият вариант е да си напишем още един клас, който да сглобява частите.
 */
class RobotDirector
{
	private RobotBuilderInterface $robotBuilder;

	public function __construct(RobotBuilderInterface $robotBuilder)
	{
		$this->robotBuilder = $robotBuilder;
	}

	public function makeRobot() : RobotBuilderInterface
	{
		return $this->robotBuilder
			->buildRobotHead()
			->buildRobotTorso()
			->buildRobotArms()
			->buildRobotLegs();
	}
}

$t1000 = new RobotDirector(new Т1000RobotBuilder(new Robot1));
var_dump($t1000->makeRobot()->getRobot());


/**
 * Robot1 е реално някакъв общ, абстрактен клас, където ще са само общите за всички роботи неща.
 * Вместо така както е - интерфейс+клас, може и да е абстрактен клас например.
 *
 * Излиза че самото създаване на конкретен робот става в Т1000RobotBuilder, 
 * и то не точно крайното създаване, а само създаването на отделните части,
 * без самото сглобяване на целия робот.
 * Демек, само задава каква да е отделната част - това ще е такова, онова ще е онакова...
 * Идеята на Билдърите като Т1000RobotBuilder по принцип е, да са все едно готови "полуфабрикати" 
 * на отделните модели роботи, които да можем да подаваме на Директор, който да ни ги сглоби.
 * Отделно, така "скриваме" как точно се прави даденият модел роботи "зад завесите" на Билдъра му.
 *
 * Сглобяването става в "Директора" - RobotDirector, на него подаваме "парчетата", "частите".
 *
 * Колкото и други различни модели роботи да ни трябват, просто трябва да си 
 * създадем съответен "Билдър" за всеки, подаваме го (Билдъра) на "Директора" и той сглобява.
 *
 * Голямото удобство на самият Builder design pattern е, че я си представете да не е предифинирано 
 * всичко в Билдъра, а трябва отвън да го подаваме като аргумент. Ако са 2, 3 ОК, но ако са много?
 *
 * Друго удобство е, че можем и да не добавяме всички части, ако искаме например 
 * робот без ръце и т.н...
 */
